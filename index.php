<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="shortcut icon" type="image/x-icon" href="/assets/img/favicon.ico">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="format-detection" content="telephone=no">

		<title>Легион Здравоохранение: создание сайтов и landing page, порталов, промо сайтов, социальных сетей, SEO, SMM, фирменный стиль, создание приложений в медицине</title>

		<meta name="description" content="Основным направлением деятельности компании является оказание рекламных услуг для различных мед. учреждений: клиники, стоматологии, поликлиники, аптеки, медцентры, НКО.">
		<meta name="keywords" content="создание сайтов, брендинг, фирменный стиль, лэндинг">

		<? require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/link_head.php');?>
		<!--if lt IE 9
		script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
		script(src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')
		-->
		<? require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/yMetrika.php');?>
	</head>
	<body>
		<div id="page" class="page">
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/header.php');?>
			<section id="content" class="content">
				<div class="index-1" style="background-image: url('/assets/img/mda_top.jpg');background-repeat: no-repeat; background-position: left top">
					<div class="wrap">
						<div class="index-1-title-bold">digital стратегии</div>
						<div class="index-1-title-light">в области здравоохранения</div>
						<div class="index-1-title-lower"><span>Подробнее о наших <a href="/services.php">сервисах</a> и <a href="/cases.php">проектах</a>.</span></div>
					</div>
				</div>
				<div class="index-6">
					<div class="wrap">
						<div class="table">
							<div class="row">
								<div class="cell w25 wow fadeInLeft" data-wow-delay="0s">
									<div class="index-6-couter">01</div>
									<div class="index-6-item">
										<div class="index-6-title">
											<div><a href="/services.php"><img src="/assets/img/index-6-2.png" alt=""></a></div>
											<div>
												Веб-разработка
											</div>
										</div>
										<div class="index-6-text">Подготовка основы для продвижения клиники или препарата в сети.</div>
									</div>
								</div>
								<div class="cell w25 wow fadeInLeft" data-wow-delay="0.1s">
									<div class="index-6-couter">02</div>
									<div class="index-6-item">
										<div class="index-6-title">
											<div><a href="/marketing.php"><img src="/assets/img/index-6-3.png" alt="" style="margin-top:-8px"></a></div>
											<div>Маркетинг</div>
										</div>
										<div class="index-6-text">Разработка и реализация стратегии продвижения продукта или услуги.</div>
									</div>
								</div>
								<div class="cell w25 wow fadeInLeft" data-wow-delay="0.2s">
									<div class="index-6-couter">03</div>
									<div class="index-6-item">
										<div class="index-6-title">
											<div><a href="/brending.php"><img src="/assets/img/index-6-1.png" alt="" style="margin-top:-8px"></a></div>
											<div>

												Брендинг
											</div>
										</div>
										<div class="index-6-text">Создание образа медцентра или лекарства. </div>
									</div>
								</div>
								<div class="cell w25 wow fadeInLeft" data-wow-delay="0.3s">
									<div class="index-6-couter">04</div>
									<div class="index-6-item">
										<div class="index-6-title">
											<div><a href="/mobile.php"><img src="/assets/img/index-6-4.png" alt="" style="margin-top:-10px"></a></div>
											<div>Приложения</div>
										</div>
										<div class="index-6-text">Увеличение лояльности к медицинскому центру и вовлеченность в лечение пациента.</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div style="background: url(/assets/img/bg-1.png) no-repeat center center;" class="index-3">
					<div class="wrap">
						<div class="table w100">
							<div class="row">
								<div style="background-image: url(/assets/img/s2.png); cursor: pointer;" class="cell w67" data-link="/alerana.php">
									<a class="wrapperedLink" href="/alerana.php">
									<div class="index-3-text wow fadeInLeft">
										<div class="services-3-title">сайт<br>маркетинг</div>
										<div class="index-3-text-title">
											<div>Продукты </div>
											<div>
												<img src="/assets/img/alerana-logo-light.png" alt="Алерана"></div>
										</div>
										<div class="index-2-text-text"><i>Сайт для серии ALERANA<br>о способах сохранения<br>здоровья волос.</i></div>
									</div>
									</a>
								</div>
								<div class="cell w33" style="cursor:pointer;" data-link="/calculator.php">
									<a class="wrapperedLink" href="/calculator.php">
									<div class="index-3-text-title wow fadeInRight">
										<div><img src="/assets/img/pfizer-logo.png" alt="Pfizer"></div>
										<div>приложение</div>
									</div>
									<div class="index-2-text-text"><br><br><i>Интерактивный калькулятор для оптимизации затрат в фарминдустрии.</i></div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div style="background: url(/assets/img/bg-2.png) no-repeat center center;" class="index-4">
					<div class="wrap">
						<div class="table w100">
							<div class="row">
								<div style="background: url(/assets/img/stelaj.png) no-repeat 0px top;cursor:pointer;" class="cell w67" data-link="/algeron.php">
									<a  class="wrapperedLink" href="/algeron.php">
									<div class="index-4-img-left wow fadeInLeft"><img src="/assets/img/index-4-6.png" alt="" style="margin-left: 35px;"></div>
									<div class="index-4-text">
										<div class="services-3-title">продвижение<br>препарата</div>
									</div>
									<div class="index-4-text-logo"><img src="/assets/img/index-4-7.png" alt=""></div>
									<div class="index-4-text-title">
										<div>Альгерон</div>
									</div>
									<div class="index-4-text-text"><i>Сайт и приложение для лечения<br>гепатита С как способ<br>продвижения препарата.</i></div>
									</a>
								</div>
								<div class="cell w33"><a class="wrapperedLink" href="/calculator.php"><img src="/assets/img/ipad.png" class="ipad"></a></div>
							</div>
						</div>
					</div>
				</div>
				<div class="index-2">
					<div class="wrap">
						<a class="wrapperedLink" href="/intan.php">
						<div class="index-2-text wow fadeInLeft">
							<div class="services-3-title">визуальная<br>коммуникация</div>
							<div class="index-2-text-logo"><img src="/assets/img/index-2-1.png" alt=""></div>
							<br>
							<div class="index-2-text-title">
								<div>Центры стоматологии</div>
								<div><b>интан</b></div>
							</div>
							<br>
							<div class="index-2-text-text"><i>Визуальная коммуникация  для крупнейшей<br>сети  стоматологических клиник  в Петербурге.</i></div>
							<!--div class="index-2-text-btn"> <a href="/intan.php" class="btn">Смотреть кейс</a></div-->
						</div>
						<div class="index-2-img wow fadeInRight"><img src="/assets/img/s1.jpg" alt=""></div>
						</a>
					</div>
				</div>
				<div class="index-5">
					<div class="wrap">
						<div class="clients">
							<div class="table w100">
								<div class="row">
									<div class="cell clients-title">Наши<br>клиенты</div>
									<div class="cell clients-items">
										<div class="table w100">
											<div class="row">
												<div class="cell">
													<img src="/assets/img/logos/Logo-01.png" alt="">
													<img src="/assets/img/logos/Logo-01_color.png" alt="">
												</div>
												<div class="cell">
													<img src="/assets/img/logos/Logo-03.png" alt="">
													<img src="/assets/img/logos/Logo-03_color.png" alt="">
												</div>
												<div class="cell">
													<img src="/assets/img/logos/Logo-02.png" alt="">
													<img src="/assets/img/logos/Logo-02_color.png" alt="">
												</div>
												<div class="cell">
													<img src="/assets/img/logos/ver-1.png" alt="">
													<img src="/assets/img/logos/ver.png" alt="">
												</div>
												<div class="cell">
													<img src="/assets/img/logos/Logo-06.png" alt="">
													<img src="/assets/img/logos/Logo-06_color.png" alt="">
												</div>
												<div class="cell">
													<img src="/assets/img/logos/Logo-07.png" alt="">
													<img src="/assets/img/logos/Logo-07_color.png" alt="">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div style="background-image: url(/assets/img/index-7-1.png);" class="index-7">
					<div class="wrap">
						<div class="table">
							<div class="row">
								<div class="cell w40 center">
									<div class="index-7-title">О компании</div>
									<div class="index-7-img"><img src="/assets/img/index-7-2.png" alt="О компании"></div>
								</div>
								<div class="cell wow fadeInUp">
									<div class="index-7-info-title">
										<div><b>digital medical</b></div>
										<div>agency</div>
									</div>
									<div class="index-7-info-text">Наша команда формирует digital в области здравоохранения. Мы верим в то, что создавая качественные продукты для наших партнеров, мы позитивно влияем на здоровье каждого человека, также как влияют на него те компании, с которыми мы работаем. Если ты разделяешь наши ценности и любишь digital так, как любим его мы - welcome on board.</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<? require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/footer.php');?>
			<? require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/modal.php');?>
			<!-- import scripts -->
			<? require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/scripts.php');?>
		</div>
	</body>
</html>
