<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="shortcut icon" type="image/x-icon" href="/assets/img/favicon.ico">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="format-detection" content="telephone=no">

		<title>Вакансии</title>
		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/link_head.php');?>
		<!--if lt IE 9
		script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
		script(src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')
		-->
		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/yMetrika.php');?>
	</head>
	<body>
		<div id="page" class="page">
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/header.php');?>
			<section id="content" class="content">
				<div class="job-1">
					<div style="background-image: url(/assets/img/job/job-1.png);" class="wrap">
						<div class="job-1-text">Работа в одном из ведущих интернет- агентств Петербурга. Интересные задачи, веселый коллектив. Удобное расположение офиса и безлимитный чай/кофе.</div>
					</div>
				</div>
				<div class="job-2">
					<div class="wrap">
						<div class="table tablerson w100">
							<div class="row">
								<div class="cell first job-2-title">
									<div>Нам</div><b>нужны</b>
								</div>
								<div class="cell who job-2-text">
									<div class="wow fadeInUp" data-wow-delay="0s"><a href="http://spb.hh.ru/vacancy/15850415?query=legion%20smm-%D1%81%D0%BF%D0%B5%D1%86%D0%B8%D0%B0%D0%BB%D0%B8%D1%81%D1%82" target="_blank"><span>SMM-специалист</span><i class="fa fa-external-link"></i></a></div>
									<div class="wow fadeInUp" data-wow-delay="0.1s"><a href="http://spb.hh.ru/vacancy/16122322?query=legion%20%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D0%BD%D0%B5%D1%82-%D0%BC%D0%B0%D1%80%D0%BA%D0%B5%D1%82%D0%BE%D0%BB%D0%BE%D0%B3" target="_blank"><span>интернет-маркетолог</span><i class="fa fa-external-link"></i></a></div>
									<div class="wow fadeInUp" data-wow-delay="0.2s"><a href="http://spb.hh.ru/vacancy/16028650?query=legion%20WEB-%D0%B4%D0%B8%D0%B7%D0%B0%D0%B9%D0%BD%D0%B5%D1%80" target="_blank"><span>WEB-дзайнер</span><i class="fa fa-external-link"></i></a></div>
								</div>
								<div class="cell tree">
									<div class="job-2-hh"><img src="/assets/img/job/hh.png" alt=""><i>Все о наших вакансиях можно найти на <a href="http://spb.hh.ru/employer/775295" target="_blank">hh.ru</a>.</i></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="job-3">
					<div class="wrap">
						<div class="table tablerson w100">
							<div class="row">
								<div class="cell first job-3-title"><span>Заявка</span></div>
								<div class="cell">
									<div class="job-thx">
										<div class="job-form-title">Спасибо</div>
										<div class="job-form-lowtitle">Мы вам скоро перезвоним</div>
									</div>
									<div class="job-form">
										<div class="job-bb">
											<div class="job-form-title">Ваше РЕЗЮМЕ</div>
											<div class="job-form-lowtitle">Мы отвечаем на все заявки без исключения</div>
											<form method="post" enctype="multipart/form-data">
												<input type="hidden" name="form_subm" value="feedback">
												<div class="table">
													<div class="row">
														<div class="cell">
															<div>
																<input type="text" name="name" required="required" placeholder="Имя" class="w100">
															</div>
															<div>
																<input type="text" name="email" placeholder="Эл. почта" class="w100 email">
															</div>
															<div>
																<input type="text" name="phone" required="required" data-inputmask-greedy="false"  data-inputmask-clearincomplete="true" placeholder="+7 (900) 000-00-00" class="w100 phones">
															</div>
														</div>
														<div class="cell">
															<textarea placeholder="Комментарий" name="text"></textarea>
														</div>
													</div>
													<div class="row">
														<div class="cell">
															<div id="captcha2"></div>
														</div>
													</div>
													<div class="row">
														<div class="cell">
															<div class="fileinput">
																<div class="fileinput-file"><span></span>
																	<input type="file" name="file" placehoder="Имя">
																	<input type="hidden" name="file_name" class="filename">
																	<input type="hidden" name="file_path" class="filepath">
																	<input type="hidden" name="file_format" class="fileformat"><a href="#"><i class="fa fa-paperclip"></i><span>Прикрепить файл</span></a>
																</div><span>не более 20 Мб</span>
															</div>
														</div>
														<div class="cell">
															<button class="btn">Отправить</button>
														</div>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/footer.php');?>

			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/modal.php');?>
			<!-- import scripts -->
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/scripts.php');?>
		</div>
	</body>
</html>
