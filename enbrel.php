<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="shortcut icon" type="image/x-icon" href="/assets/img/favicon.ico">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="format-detection" content="telephone=no">

		<title>Энбрел</title>
		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/link_head.php');?>
		<!--if lt IE 9
		script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
		script(src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')
		-->
		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/yMetrika.php');?>
	</head>
	<body>
		<div id="page" class="page">
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/header.php');?>
			<section id="content" class="content">
				<div class="enbrel">
					<div style="background: url(/assets/img/enbrel/1.png) no-repeat center" class="case-1">
						<div class="wrap">
							<div class="table">
								<div class="row">
									<div class="cell case-1-title"><span>кейс</span></div>
									<div class="cell case-1-about">
										<div>Комплексное продвижение препарата</div><b>энбрел</b>
									</div>
								</div>
								<div class="row">
									<div style="vertical-align: top;" class="cell case-1-title"><span>клиент</span></div>
									<div style="vertical-align: top;" class="cell case-1-about"><img src="/assets/img/enbrel/log.png" alt=""></div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-2">
						<div class="wrap">
							<div class="table w100">
								<div class="row">
									<div class="cell w33 wow fadeInLeft" data-wow-delay="0s">
										<div class="cases-2-item">
											<div class="index-6-couter">01</div>
											<div class="ib">
												<div class="cases-2-item-title">Задача</div>
												<div class="cases-2-item-text">Продемонстрировать целевой аудитории преимущества препарата</div>
											</div>
										</div>
									</div>
									<div class="cell w33 wow fadeInLeft" data-wow-delay="0.1s">
										<div class="cases-2-item">
											<div class="index-6-couter">02</div>
											<div class="ib">
												<div class="cases-2-item-title">Концепция</div>
												<div class="cases-2-item-text">Вовлечение пользователей в процесс оценки и мониторинга состояния своего здоровья</div>
											</div>
										</div>
									</div>
									<div class="cell w33 wow fadeInLeft" data-wow-delay="0.2s">
										<div class="cases-2-item">
											<div class="index-6-couter">03</div>
											<div class="ib">
												<div class="cases-2-item-title">Решение</div>
												<div class="cases-2-item-text">Сайт и мобильное приложение с возможностью вести дневник пациента</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div style="background-color: #fff;" class="case-3">
						<div style="background-image: url(/assets/img/enbrel/2.png);" class="wrap"></div>
					</div>
					<div class="case-6">
						<div class="table w100 h800">
							<div class="row">
								<div style="background-image: url(/assets/img/enbrel/3.png); background-size: 40%; background-position: center; background-repeat: no-repeat;" class="cell w50 case-img">
									<div class="wraptwo"></div>
								</div>
								<div style="background: #E8E8E8" class="cell w50 case-textb">
									<div class="wraptwo wow fadeInRight">
										<div class="case-title">мобильное<br>приложение</div>
										<div class="case-label">
											<div>Дневник </div><b>пациента</b>
										</div>
										<div class="case-text">
											<p>Специальное приложение позволяет пользователям вести дневник пациента.</p>
											<p>
												Простое и удобное в использовании, оно не дает забыть об очередном приеме лекарств или перепутать дозировку.


											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-7">
						<div class="table w100 h800">
							<div class="row">
								<div style="background: #DCE6F0" class="cell w50 case-textb">
									<div class="wraptwo wow fadeInLeft">
										<div class="case-title">мобильное<br>приложение</div>
										<div class="case-label">
											<div>прототипы</div><b>экранов</b>
										</div>
										<div class="case-text">
											<p>Для того чтобы приложение получилось максимально удобным, были созданы прототипы экранов, проработаны различные варианты пользовательских сценариев.</p>
										</div>
									</div>
								</div>
								<div style="background-image: url(/assets/img/enbrel/4.png); background-size: initial; background-position: center; background-repeat: no-repeat;" class="cell w50 case-img">
									<div class="wraptwo"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-9">
						<div class="table w100 h800">
							<div class="row">
								<div style="background-image: url(/assets/img/enbrel/5.png); background-size: 80%; background-position: center; background-repeat: no-repeat;" class="cell w50 case-img"></div>
								<div style="background: #DCE6F0" class="cell w50 case-textb">
									<div class="wraptwo wow fadeInRight">
										<div class="case-title">Веб-версия</div>
										<div class="case-label">
											<div>веб-</div><b>версия</b>
										</div>
										<div class="case-text">
											<p>Веб-версия позволяет продолжать ведение дневника, даже если у пользователя нет гаджета под рукой. Еженедельные тесты на индексы BASDAI и BASFI позволяют отследить динамику состояния, а регулярные новости держат пациентов в курсе событий.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-7">
						<div class="table w100 h800">
							<div class="row">
								<div style="background: #E8E8E8" class="cell w50 case-textb">
									<div class="wraptwo wow fadeInLeft">
										<div class="case-title">мобильное<br>приложение</div>
										<div class="case-label">
											<div>оценка</div><b>здоровья</b>
										</div>
										<div class="case-text">
											<p>Тесты на индексы BASDAI и BASFI позволяют пациенту оценить состояние здоровья. </p>
											<p>
												При отклонениях от нормы программа советует обратиться к врачу. Простой и понятный интерфейс не позволит запутаться даже тем, кто ничего не знает о болезни.

											</p>
										</div>
									</div>
								</div>
								<div style="background-image: url(/assets/img/enbrel/6.png); background-size: 60%; background-position: center; background-repeat: no-repeat;" class="cell w50 case-img">
									<div class="wraptwo"></div>
								</div>
							</div>
						</div>
					</div>
					<div style="background: white" class="case-11">
						<div style="background-image: url(/assets/img/enbrel/7.png); background-repeat: no-repeat; background-position: center" class="wrap"></div>
					</div>
					<div class="case-13">
						<div class="table w100 h800">
							<div class="row">
								<div style="background-image: url(/assets/img/enbrel/8.png); background-size: cover; background-repeat: no-repeat; background-position: center" class="cell w50 case-img"></div>
								<div style="background: rgba(159, 208, 234, 0.25)" class="cell w50 case-textb">
									<div class="wraptwo wow fadeInRight">
										<div class="case-title">веб-разрботка</div>
										<div class="case-label">
											<div>медицинский</div><b>тест</b>
										</div>
										<div class="case-text">
											<p>Медицинский тест на оценку здоровья привлекает целевую аудиторию, заинтересованную в препарате «Энбрел» как пути решения проблем со здоровьем.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-14">
						<div class="table w100 h800">
							<div class="row">
								<div style="background: rgba(159, 208, 234, 0.25)" class="cell w50 case-textb">
									<div class="wraptwo wow fadeInLeft">
										<div class="case-title">веб-разрботка</div>
										<div class="case-label">
											<div>Промо-сайт </div><b>препаратов</b>
										</div>
										<div class="case-text">
											<p>На сайте собраны материалы о препарате, инструкции по его применению, информация по противопоказаниям, дозировкам и оценкам эффективности лекарства. </p>
											<p>Пользователь может узнать о пяти заболеваниях, для борьбы с которыми создан «Энбрел».</p>
										</div>
									</div>
								</div>
								<div style="background-image: url(/assets/img/enbrel/9.png); background-size: inherit; background-position: center; background-repeat: no-repeat;" class="cell w50 case-img"></div>
							</div>
						</div>
					</div>
					<div class="case-14">
						<div class="table w100 h800">
							<div class="row">
								<div style="background-image: url(/assets/img/enbrel/10.png); background-size: cover; background-repeat: no-repeat; background-position: center" class="cell w50 case-img">
									<div class="wraptwo"></div>
								</div>
								<div style="background: rgba(159, 208, 234, 0.25)" class="cell w50 case-textb">
									<div class="wraptwo wow fadeInRight">
										<div class="case-title">веб-разрботка</div>
										<div class="case-label">
											<div>База </div><b>знаний</b>
										</div>
										<div class="case-text">
											<p>Ресурс интересен не только для пациентов, но и для профессионального сообщества.</p>
											<p>На сайте собрано и обработано более 200 страниц информационных материалов для врачей и медицинских представителей.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-19">
						<div class="wrap center">
							<!-- Slider main container-->
							<div class="swiper-container">
								<!-- Additional required wrapper-->
								<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/slides.php');?>
								<!-- If we need navigation buttons-->
								<div class="swiper-button-prev"></div>
								<div class="swiper-button-next"></div>
							</div>
						</div>
					</div>
					<div class="services-3">
						<div style="background-image: url(/assets/img/mda_bottom.jpg);" class="wrap">
							<div class="services-3-title">обратная связь</div>
							<div class="services-3-about">
								<div class="services-3-about-title">О возможных противопоказаниях </div>
								<div class="services-3-about-text">проконсультируйтесь<br>у нашего специалиста </div>
								<div class="services-3-about-btn"><a href="#order" class="btn blue fancy">Заказать услугу</a></div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/footer.php');?>
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/modal.php');?>
			<!-- import scripts -->
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/scripts.php');?>
		</div>
	</body>
</html>
