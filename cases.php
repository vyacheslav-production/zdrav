<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="shortcut icon" type="image/x-icon" href="/assets/img/favicon.ico">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="format-detection" content="telephone=no">

		<title>Портфолио: создание сайтов и landing page, порталов, промо сайтов, социальных сетей, SEO, SMM, фирменный стиль, создание приложений - Легион Здравоохранение</title>
		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/link_head.php');?>
		<!--if lt IE 9
		script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
		script(src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')
		-->
		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/yMetrika.php');?>
	</head>
	<body>
		<div id="page" class="page">
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/header.php');?>
			<section id="content" class="content">
				<div class="cases-1">
					<div class="wrap">
						<div class="table w100">
							<div class="row">
								<div style="background-image: url(/assets/img/cases/2.png); cursor: pointer;" class="cell cases-1-left" data-link="/nobel.php">
									<a class="wrapperedLink" href="/nobel.php">
									<div class="cases-1-title wow fadeInLeft">
										<div>intan </div><b>nobel</b>
									</div>
									<div class="cases-1-text wow fadeInLeft">Установка имплантатов высокой биологической  совместимости.</div>
									<div class="cases-1-label wow fadeInLeft">лендинг</div>
									<div class="cases-1-img wow fadeInLeft"><img src="/assets/img/cases/3.png" alt=""><img src="/assets/img/cases/4.png" alt=""></div>
									</a>
								</div>
								<div style="background-image: url(/assets/img/phone.png); cursor: pointer;" class="cell w33 cases-1-right"  data-link="/algeron.php">
									<a href="/algeron.php">
									<div class="cases-1-title wow fadeInRight"><b>Альгерон</b></div>
									<div class="cases-1-text wow fadeInRight">Мобильное приложение  для отслеживания курса  терапии.</div>
									<div class="cases-1-label wow fadeInRight">приложение</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="cases-2">
					<div class="wrap">
						<div class="table w100">
							<div class="row">
								<div style="background:url('/assets/img/cases/hears.png') no-repeat center center; cursor: pointer;" class="cell w30" data-link="/alerana.php">
									<a href="/alerana.php">
									<div class="cases-2-item">
										<div class="cases-2-item-title wow fadeInLeft">АЛЕРАНА</div>
										<div class="cases-2-item-text wow fadeInLeft">Сайт для серии ALERANA о способах  сохранения здоровья волос.</div>
										<div class="cases-2-item-type wow fadeInLeft"> <span>Сайт </span></div>
									</div>
									</a>
								</div>
								<div style="background:url('/assets/img/cases/water.png') no-repeat center center;background-size:cover;cursor:pointer;" class="cell w30" data-link="/acellbiya.php">
									<a href="/acellbiya.php">
									<div class="cases-2-item">
										<div class="cases-2-item-title wow fadeInUp"> АЦЕЛЛБИЯ</div>
										<div class="cases-2-item-text wow fadeInUp">Промо-сайт для продвижения противовирусного препарата.</div>
										<div class="cases-2-item-type wow fadeInUp"> <span>Сайт</span></div>
									</div>
									</a>
								</div>
								<div style="background:url('/assets/img/cases/woman.png') no-repeat center center;background-size:100%;cursor:pointer;" class="cell w40 black" data-link="/enbrel.php">
									<a href="/enbrel.php">
									<div class="cases-2-item">
										<div class="cases-2-item-title wow fadeInRight"> Энбрел</div>
										<div class="cases-2-item-text wow fadeInRight">Сайт о преимуществах препарата<br>для лечения ревматоидного артрита.</div>
										<div class="cases-2-item-type wow fadeInRight"> <span>Сайт</span></div>
									</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="cases-3">
					<div class="wrap">
						<div class="table w100">
							<div class="row">
								<div class="cell w60 bgs">
									<div class="cases-3-title wow fadeInLeft">создано<br>более</div>
									<div class="cases-3-counter wow fadeInLeft"><b>40 digital </b>
										<div>проектов</div>
									</div>
									<div class="cases-3-label"><span>Лидеры рынка  фармацевтики</span></div>
									<div class="cases-3-btn"> <a href="#order" class="btn light fancy">Заказать услугу</a></div>
								</div>
								<div style="background: #009FDA url('/assets/img/cases/pfizer.png') no-repeat right center;cursor:pointer;" class="cell w40" data-link="/calculator.php">
									<a href="/calculator.php">
									<div class="cases-2-item wow fadeInRight">
										<div class="cases-2-item-title"> PFIZER</div>
										<div class="cases-2-item-text">Интерактивный калькулятор<br>для оптимизации затрат в фарминдустрии.</div>
										<div class="cases-2-item-type"> <span>Приложение</span></div>
									</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="cases-4">
					<div class="wrap">
						<div class="table w100">
							<div class="row">
								<div style="background: #fef2e2 url('/assets/img/cases/child.png') no-repeat -35px -10px;cursor:pointer;" class="cell w30" data-link="/genferon.php">
									<a href="/genferon.php">
									<div class="cases-2-item wow fadeInLeft">
										<div style="color:#3B393D;" class="cases-2-item-title"> Генферон</div>
										<div style="color:#3B393D;" class="cases-2-item-text">Промо-сайт для продвижения противовирусного препарата.</div>
										<div style="color:#3B393D;" class="cases-2-item-type"> <span>промо-Сайт<br>Маркетинг</span></div>
									</div>
									</a>
								</div>
								<div style="background: #fef2e2 url('/assets/img/cases/forest.png') no-repeat center center;background-size:cover;cursor:pointer;" class="cell w30" data-link="/yanvirus.php">
									<a href="/yanvirus.php">
									<div class="cases-2-item wow fadeInUp">
										<div class="cases-2-item-title"> яквинус</div>
										<div class="cases-2-item-text">Информационный сайт о способах лечения болезни Бехтерева.</div>
										<div class="cases-2-item-type"> <span>Сайт<br>Маркетинг</span></div>
									</div>
									</a>
								</div>
								<div style="background: #CDEBDE url('/assets/img/cases/tooth.png') no-repeat 90px center;background-size:cover;cursor:pointer;" class="cell w40" data-link="/intan.php">
									<a href="/intan.php">
									<div class="cases-2-item  wow fadeInRight">
										<div class="cases-2-item-title"> Интан</div>
										<div class="cases-2-item-text">Визуальная коммуникация<br>и продвижение для крупнейшей сети стоматологических клиник<br>в Петербурге.</div>
										<div class="cases-2-item-type"> <span>Сайт</span></div>
									</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="about-9">
					<div class="wrap" style="background: none; padding-top: 0px;">
						<div class="table tablerson w100" style="background: #b7d9ea; padding: 66px 15px;">
							<div class="row">
								<div class="cell">
									<div class="table w100">
										<div class="row">
											<div class="cell first job-3-title" style="width:220px;">
												<div class="cases-2-item-text" style="padding-top:5px;color: #3b393d; text-transform: none; height: auto; width:100%; padding-top 0px;">
													<span>Познакомьтесь с нами ближе</span>
												</div>
											</div>
											<div class="cell to-pdf right"><a href="/upload/pdf/farm_prez.pdf" download="/upload/pdf/farm_prez.pdf" tagret="_blank" class="link-to-pdf left"><i class="fa fa-file-pdf-o"></i><span><span>На Русском</span><i>9.4 Мб</i></span></a><a href="/upload/pdf/Legion_final_eng.pdf" download="/upload/pdf/Legion_final_eng.pdf" tagret="_blank" class="link-to-pdf left"><i class="fa fa-file-pdf-o"></i><span><span>in english</span><i>6.5 Mb</i></span></a><a href="/upload/pdf/Legion_france.pdf" download="/upload/pdf/Legion_france.pdf" tagret="_blank" class="link-to-pdf left"><i class="fa fa-file-pdf-o"></i><span><span>en franÇaise</span><i>9.5 Mb</i></span></a></div>
										</div>
									</div>
								</div>
								<div style="width: 345px;" class="cell index-2-text-title">
									<div class="cases-3-btn" style="padding-top:0px;">
										<a href="#order" class="btn light fancy">Заказать услугу</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/footer.php');?>

			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/modal.php');?>

			<!-- import scripts -->
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/scripts.php');?>
		</div>
	</body>
</html>
