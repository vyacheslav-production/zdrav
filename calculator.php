<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="shortcut icon" type="image/x-icon" href="/assets/img/favicon.ico">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="format-detection" content="telephone=no">
		<title>Калькулятор</title>
		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/link_head.php');?>
		<!--if lt IE 9
		script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
		script(src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')
		-->
		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/yMetrika.php');?>
	</head>
	<body>
		<div id="page" class="page">
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/header.php');?>
			<section id="content" class="content">
				<div class="calculator">
					<div style="background: url(/assets/img/calculator/1.png) no-repeat center" class="case-1">
						<div class="wrap">
							<div class="table">
								<div class="row">
									<div class="cell case-1-title"><span>кейс</span></div>
									<div class="cell case-1-about">
										<div class="white">интерактивный</div><b class="white">калькулятор</b>
									</div>
								</div>
								<div class="row">
									<div style="vertical-align: top;" class="cell case-1-title"><span>клиент</span></div>
									<div style="vertical-align: top;" class="cell case-1-about"><img src="/assets/img/calculator/log.png" alt=""></div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-2">
						<div class="wrap">
							<div class="table w100">
								<div class="row">
									<div class="cell w33 wow fadeInLeft" data-wow-delay="0s">
										<div class="cases-2-item">
											<div class="index-6-couter">01</div>
											<div class="ib">
												<div class="cases-2-item-title">Задача</div>
												<div class="cases-2-item-text">Создание удобного инструмента для расчета прямых затрат на фармакотерапию ревматоидного артрита</div>
											</div>
										</div>
									</div>
									<div class="cell w33 wow fadeInLeft" data-wow-delay="0.1s">
										<div class="cases-2-item">
											<div class="index-6-couter">02</div>
											<div class="ib">
												<div class="cases-2-item-title">Концепция</div>
												<div class="cases-2-item-text">Конструирование простого в использовании интерактивного калькулятора</div>
											</div>
										</div>
									</div>
									<div class="cell w33 wow fadeInLeft" data-wow-delay="0.2s">
										<div class="cases-2-item">
											<div class="index-6-couter">03</div>
											<div class="ib">
												<div class="cases-2-item-title">Решение</div>
												<div class="cases-2-item-text">Создание нативного мобильного приложения</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div style="background-color: #fff;" class="case-3">
						<div style="background-image: url(/assets/img/calculator/2.png); background-size: cover" class="wrap"></div>
					</div>
					<div class="case-7">
						<div class="table w100 h800">
							<div class="row">
								<div style="background-image: url(/assets/img/calculator/3.png); background-size: 80%; background-position: center; background-repeat: no-repeat; background-color: #fff" class="cell w50 case-img">
									<div class="wraptwo"></div>
								</div>
								<div style="background: #E8E8E8" class="cell w50 case-textb">
									<div class="wraptwo wow fadeInRight">
										<div class="case-title">Веб-приложение</div>
										<div class="case-label">
											<div>Возможности </div><b>приложения</b>
										</div>
										<div class="case-text">
											<p>Интерактивный калькулятор предназначен для фармаэкономической оценки масштабов использования лекарственных препаратов при лечении ревматоидного артрита в разных регионах страны. </p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-6">
						<div class="table w100 h800">
							<div class="row">
								<div style="background: #D6E7F0" class="cell w50 case-textb">
									<div class="wraptwo wow fadeInLeft">
										<div class="case-title">Веб-приложение</div>
										<div class="case-label"><b>Аудитория</b></div>
										<div class="case-text">
											<p>Веб-приложение используется профессионалами от фарминдустрии. Данные по применению лекарственных средств заносятся лечащими врачами в базу данных, а затем оцениваются маркетологами, что позволяет оптимизировать затраты на их производство и продажу.</p>
										</div>
									</div>
								</div>
								<div style="background-image: url(/assets/img/calculator/4.png); background-size: 90%; background-position: center; background-repeat: no-repeat;" class="cell w50 case-img"></div>
							</div>
						</div>
					</div>
					<div class="case-9">
						<div class="table w100 h800">
							<div class="row">
								<div style="background-image: url(/assets/img/calculator/5.png); background-size: cover; background-position: top center; background-repeat: no-repeat;" class="cell w50 case-img">
								</div>
								<div style="background: #E8E8E8" class="cell w50 case-textb">
									<div class="wraptwo wow fadeInRight">
										<div class="case-title">Веб-приложение</div>
										<div class="case-label">
											<div>Функционал  </div><b>приложения</b>
										</div>
										<div class="case-text">
											<p>Интерактивный калькулятор позволяет провести расчет прямых затрат на фармакотерапию ревматоидного артрита, учитывая при этом затраты на вспомогательные генно-инженерные биологические препараты, а также издержки на визиты к врачу.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-6">
						<div class="table w100 h800">
							<div class="row">
								<div style="background: #D6E7F0" class="cell w50 case-textb">
									<div class="wraptwo wow fadeInLeft">
										<div class="case-title">Веб-приложение</div>
										<div class="case-label">
											<div>Географические </div><b>рамки</b>
										</div>
										<div class="case-text">
											<p>
												Интерактивный калькулятор используется врачами в разных регионах страны, что позволяет собирать полную статистику использования лекарственных средств при лечении ревматоидного артрита по всей России.

											</p>
										</div>
									</div>
								</div>
								<div style="background-image: url(/assets/img/calculator/6.png); background-size: initial; background-position: bottom center; background-repeat: no-repeat;" class="cell w50 case-img"></div>
							</div>
						</div>
					</div>
					<div class="case-19">
						<div class="wrap center">
							<!-- Slider main container-->
							<div class="swiper-container">
								<!-- Additional required wrapper-->
								<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/slides.php');?>
								<!-- If we need navigation buttons-->
								<div class="swiper-button-prev"></div>
								<div class="swiper-button-next"></div>
							</div>
						</div>
					</div>
					<div class="services-3">
						<div style="background-image: url(/assets/img/mda_bottom.jpg);" class="wrap">
							<div class="services-3-title">обратная связь</div>
							<div class="services-3-about">
								<div class="services-3-about-title">О возможных противопоказаниях </div>
								<div class="services-3-about-text">проконсультируйтесь<br>у нашего специалиста </div>
								<div class="services-3-about-btn"><a href="#order" class="btn blue fancy">Заказать услугу</a></div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/footer.php');?>
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/modal.php');?>
			<!-- import scripts -->
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/scripts.php');?>
		</div>
	</body>
</html>
