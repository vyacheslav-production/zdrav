
<header id="header" class="header default">
	<div class="wrap">
		<div class="table">
			<div class="row">
				<div class="cell header-logo"><a href="/index.php"><img src="/assets/img/logo_legion_nw.png" alt=""/></a></div>
				<div class="cell header-nav center">
					<div class="table w100">
						<ul class="row">
							<?php
								$cases = false;
								$services = false;
								switch($_SERVER['PHP_SELF']){
									case '/alerana.php':
									case '/algeron.php':
									case '/acellbiya.php':
									case '/enbrel.php':
									case '/calculator.php':
									case '/genferon.php':
									case '/yanvirus.php':
									case '/intan.php':
									case '/nobel.php':
										$cases = true;
										break;
									case '/marketing.php':
									case '/brending.php':
									case '/mobile.php':
										$services = true;
										break;
								}
							?>
							<li class="cell"><a <?=($_SERVER['PHP_SELF'] == "/services.php" || $services)? 'class="active"': ''?> href="/services.php">УСЛУГИ</a></li>
							<li class="cell"><a <?=($_SERVER['PHP_SELF'] == "/cases.php" || $cases)? 'class="active"': ''?>href="/cases.php">ПОРТФОЛИО</a></li>
							<li class="cell"><a <?=($_SERVER['PHP_SELF'] == "/about.php")? 'class="active"': ''?>href="/about.php">О КОМПАНИИ</a></li>
							<li class="cell"><a <?=($_SERVER['PHP_SELF'] == "/job.php")? 'class="active"': ''?>href="/job.php">ВАКАНСИИ</a></li>
							<li class="cell"><a <?=($_SERVER['PHP_SELF'] == "/contact.php")? 'class="active"': ''?>href="/contact.php">КОНТАКТЫ</a></li>
							<li class="cell"><a <?=($_SERVER['PHP_SELF'] == "/seminar.php")? 'class="active"': ''?>href="/seminar.php">СЕМИНАР</a></li>
						</ul>
					</div>
				</div>
				<div class="cell header-btn"><a href="#order" class="btn light fancy">Заказать услугу</a></div>
			</div>
		</div>
	</div>
</header>
