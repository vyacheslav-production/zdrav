
<div class="hidden">
	<div id="order" class="job-3">
		<div class="job-thx"><a href="#" class="fancy-close"><i class="fa fa-times"></i></a>
			<div class="job-form-title">Спасибо</div>
			<div class="job-form-lowtitle">Мы вам скоро перезвоним</div>
		</div>
		<div class="job-form"><a href="#" class="fancy-close"><i class="fa fa-times"></i></a>
			<div class="job-form-title">свяжитесь с нами</div>
			<div class="job-form-lowtitle">Мы отвечаем на все заявки без исключения</div>
			<form method="post" enctype="multipart/form-data">
				<input type="hidden" name="form_subm" value="feedback">
				<div class="table">
					<div class="row">
						<div class="cell">
							<div>
								<input type="text" name="name" required="required" placeholder="Имя" class="w100"/>
							</div>
							<div>
								<input type="text" name="email" placeholder="Эл. почта" class="w100 email"/>
							</div>
							<div>
								<input type="text" name="phone" required="required"  data-inputmask-greedy="false"  data-inputmask-clearincomplete="true"  placeholder="+7 (900) 000-00-00" class="w100 phones"/>
							</div>
						</div>
						<div class="cell">
							<textarea placeholder="Комментарий" name="text"></textarea>
						</div>
					</div>
					<div class="row">
						<div class="cell">
							<div id="captcha"></div>
						</div>
					</div>
					<div class="row">
						<div class="cell">
							<div class="fileinput">
								<div class="fileinput-file"><span></span>
									<input type="file" name="file" placehoder="Имя"/>
									<input type="hidden" name="file_name" class="filename"/>
									<input type="hidden" name="file_path" class="filepath"/>
									<input type="hidden" name="file_format" class="fileformat"/><a href="#"><i class="fa fa-paperclip"></i><span>Прикрепить файл</span></a>
								</div><span>не более 20 Мб</span>
							</div>
						</div>
						<div class="cell">
							<button class="btn">Отправить</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div id="brending" class="job-4">
		<div class="job-thx"><a href="#" class="fancy-close"><i class="fa fa-times"></i></a>
			<div class="job-form-title">Спасибо</div>
			<div class="job-form-lowtitle">Мы вам скоро перезвоним</div>
		</div><a href="#" class="fancy-close"><i class="fa fa-times"></i></a>
		<div class="job-form">
			<div class="job-form-title">Брендинг</div>
			<form action="">
				<input type="hidden" name="form_subm" value="services">
				<div class="table">
					<div class="row">
						<div class="cell">
							<div><span class="job-form-quest">Существует ли у вас фирменный стиль?</span><br/>
								<input type="hidden" name="question-1[]" value="Существует ли у вас фирменный стиль?">
								<input type="text" name="question-1[]" required="required" placeholder="Нет, но нам он необходим." class="w100"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="cell">
							<div><span class="job-form-quest">Есть ли у вас веб-ресурс?</span><br/>
								<input type="hidden" name="question-2[]" value="Есть ли у вас веб-ресурс?">
								<input type="text" name="question-2[]" required="required" placeholder="Есть, но его функционал нас не полностью устраивает..." class="w100"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="cell">
							<div><br/>
								<input type="text" name="contact" required="required" placeholder="Оставьте ваш номер телефона или почту" class="w100"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="cell"><br/><br/>
							<div id="captcha4" style="float:left"></div>
							<br>
							<button class="btn">Отправить</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div id="web" class="job-4">
		<div class="job-thx"><a href="#" class="fancy-close"><i class="fa fa-times"></i></a>
			<div class="job-form-title">Спасибо</div>
			<div class="job-form-lowtitle">Мы вам скоро перезвоним</div>
		</div><a href="#" class="fancy-close"><i class="fa fa-times"></i></a>
		<div class="job-form">
			<div class="job-form-title">ВЕБ-РАЗРАБОТКА</div>
			<form action="">
				<input type="hidden" name="form_subm" value="services">
				<div class="table">
					<div class="row">
						<div class="cell">
							<div><span class="job-form-quest">Есть ли у вас веб-ресурс?</span><br/>
								<input type="hidden" name="question-1[]" value="Есть ли у вас веб-ресурс?">
								<input type="text" name="question-1[]" required="required" placeholder="Есть, но его функционал нас не полностью устраивает..." class="w100"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="cell">
							<div><span class="job-form-quest">Существует ли у вас фирменный стиль?</span><br/>
								<input type="hidden" name="question-2[]" value="Существует ли у вас фирменный стиль?">
								<input type="text" name="question-2[]" required="required" placeholder="Да, но хотелось бы его пересмотреть…" class="w100"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="cell">
							<div><br/>
								<input type="text" name="contact" required="required" placeholder="Оставьте ваш номер телефона или почту" class="w100"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="cell"><br/><br/>
							<div id="captcha2" style="float:left"></div>
							<br>
							<button class="btn">Отправить</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div id="marketing" class="job-4">
		<div class="job-thx"><a href="#" class="fancy-close"><i class="fa fa-times"></i></a>
			<div class="job-form-title">Спасибо</div>
			<div class="job-form-lowtitle">Мы вам скоро перезвоним</div>
		</div><a href="#" class="fancy-close"><i class="fa fa-times"></i></a>
		<div class="job-form">
			<div class="job-form-title">Интернет-маркетинг</div>
			<form action="">
				<input type="hidden" name="form_subm" value="services">
				<div class="table">
					<div class="row">
						<div class="cell">
							<div><span class="job-form-quest">Используете ли вы SMM?</span><br/>
								<input type="hidden" name="question-1[]" value="Используете ли вы SMM?">
								<input type="text" name="question-1[]" required="required" placeholder="Нет, но хотелось бы..." class="w100"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="cell">
							<div><span class="job-form-quest">Используете ли вы SEO-оптимизацию?</span><br/>
								<input type="hidden" name="question-2[]" value="Используете ли вы SEO-оптимизацию?">
								<input type="text" name="question-2[]" required="required" placeholder="Да, но хотелось бы большего эффекта..." class="w100"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="cell">
							<div> <br/>
								<input type="text" name="contact" required="required" placeholder="Оставьте ваш номер телефона или почту" class="w100"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="cell"><br/><br/>
							<div id="captcha3" style="float:left"></div>
							<br>
							<button class="btn">Отправить</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div id="mobile" class="job-4">
		<div class="job-thx"><a href="#" class="fancy-close"><i class="fa fa-times"></i></a>
			<div class="job-form-title">Спасибо</div>
			<div class="job-form-lowtitle">Мы вам скоро перезвоним</div>
		</div><a href="#" class="fancy-close"><i class="fa fa-times"></i></a>
		<div class="job-form">
			<div class="job-form-title">Мобильные приложения</div>
			<form action="">
				<input type="hidden" name="form_subm" value="services">
				<div class="table">
					<div class="row">
						<div class="cell">
							<div><span class="job-form-quest">Есть ли у вас веб-ресурс?</span><br/>
								<input type="hidden" name="question-1[]" value="Есть ли у вас веб-ресурс?">
								<input type="text" name="question-1[]" required="required" placeholder="Да, но..." class="w100"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="cell">
							<div><span class="job-form-quest">Существует ли у вас фирменный стиль?</span><br/>
								<input type="hidden" name="question-2[]" value="Существует ли у вас фирменный стиль?">
								<input type="text" name="question-2[]" required="required" placeholder="Нет, но нам он необходим." class="w100"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="cell">
							<div> <br/>
								<input type="text" name="contact" required="required" placeholder="Оставьте ваш номер телефона или почту" class="w100"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="cell"><br/><br/>
							<div id="captcha5" style="float:left"></div>
							<br>
							<button class="btn">Отправить</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
