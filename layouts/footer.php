
<footer id="footer" class="footer">
	<div class="wrap">
		<div class="table w100">
			<div class="row">
				<div class="cell footer-copy"><a href="http://legion.info/" target="_blank"><img src="/assets/img/copyright.png" alt=""/></a>
					<div>© ИНТЕРНЕТ-АГЕНТСТВО «ЛЕГИОН»<br/>2011-2016</div>
				</div>
				<div class="cell footer-social center">
					<noindex>
						<a href="http://vk.com/legionspb" target="_blank" rel="nofollow"><i class="fa fa-vk"></i></a>
						<a href="http://instagram.com/legion_digital" target="_blank" rel="nofollow"><i class="fa fa-instagram"></i></a>
						<a href="https://www.facebook.com/legionspb" target="_blank" rel="nofollow"><i class="fa fa-facebook"></i></a>
					</noindex>
				</div>
				<div class="cell header-nav footer-nav center">
					<div class="table w100">
						<ul class="row">
							<li class="cell center"><a href="/services.php">УСЛУГИ</a></li>
							<li class="cell center"><a href="/cases.php">ПОРТФОЛИО</a></li>
							<li class="cell center"><a href="/about.php">О&nbsp;КОМПАНИИ</a></li>
							<li class="cell center"><a href="/job.php">ВАКАНСИИ</a></li>
							<li class="cell center"><a href="/contact.php">КОНТАКТЫ</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
