<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="shortcut icon" type="image/x-icon" href="/assets/img/favicon.ico">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="format-detection" content="telephone=no">

		<title>ИНТАН & NOBEL</title>
		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/link_head.php');?>
		<!--if lt IE 9
		script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
		script(src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')
		-->
		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/yMetrika.php');?>
	</head>
	<body>
		<div id="page" class="page">
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/header.php');?>
			<section id="content" class="content">
				<div class="nobel">
					<div style="background-image: url(/assets/img/nobel/header.png); background-repeat: no-repeat; background-position: center" class="case-1">
						<div style=" " class="wrap">
							<div class="table">
								<div class="row">
									<div class="cell case-1-title"><span>кейс</span></div>
									<div class="cell case-1-about">
										<div>посадочная страница</div><b>интан & nobel</b>
									</div>
								</div>
								<div class="row">
									<div class="cell case-1-title"><span>клиент</span></div>
									<div class="cell case-1-about"><img src="/assets/img/nobel/logo_intan.png" alt="">&nbsp;&nbsp;&nbsp;&nbsp;<img src="/assets/img/nobel/logo_nobel.png" alt=""></div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-2">
						<div class="wrap">
							<div class="table w100">
								<div class="row">
									<div class="cell w33 wow fadeInLeft" data-wow-delay="0s">
										<div class="cases-2-item">
											<div class="index-6-couter">01</div>
											<div class="ib">
												<div class="cases-2-item-title">Задача</div>
												<div class="cases-2-item-text">Создание лендинга для партнерской акции компании «Nobel Biocare»</div>
											</div>
										</div>
									</div>
									<div class="cell w33 wow fadeInLeft" data-wow-delay="0.1s">
										<div class="cases-2-item">
											<div class="index-6-couter">02</div>
											<div class="ib">
												<div class="cases-2-item-title">Концепция</div>
												<div class="cases-2-item-text">Соединение фирменных стилей компаний«Интан» и «Nobel Biocare»</div>
											</div>
										</div>
									</div>
									<div class="cell w33 wow fadeInLeft" data-wow-delay="0.2s">
										<div class="cases-2-item">
											<div class="index-6-couter">03</div>
											<div class="ib">
												<div class="cases-2-item-title">Решение</div>
												<div class="cases-2-item-text">Конструирование яркой и доступной посадочной страницы</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div style="background-color: #fff;" class="case-3">
						<div style="background-image: url(/assets/img/nobel/header_1.png); background-color: #F6F6F6; background-repeat: no-repeat; background-position: center" class="wrap"></div>
					</div>
					<div class="case-6">
						<div class="table w100 h800">
							<div class="row">
								<div class="cell w50 case-textb">
									<div class="wraptwo  wow fadeInLeft">
										<div class="case-title">Посадочная страница</div>
										<div class="case-label">
											<div>Лаконичный
											</div><b>дизайн</b>
										</div>
										<div class="case-text">
											<p>
												Анимация на первом экране представляет пользователю два бренда: «Интан» и «Nobel Biocare».
											</p>
											<p>
												Яркий и лаконичный дизайн концентрирует внимание посетителя на акции.
											</p>
										</div>
									</div>
								</div>
								<div class="cell w50 case-img">
									<div class="wraptwo"><img src="/assets/img/nobel/case-1.png" alt=""></div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-9">
						<div class="table w100 h800">
							<div class="row">
								<div class="cell w50 case-img">
									<div class="wraptwo"><img src="/assets/img/nobel/case-2.png" alt=""></div>
								</div>
								<div class="cell w50 case-textb">
									<div class="wraptwo  wow fadeInRight">
										<div class="case-title">Посадочная страница</div>
										<div class="case-label">
											<div>Информативность</div>
										</div>
										<div class="case-text">
											<p>Посетитель, не знакомый с компанией «Nobel Biocare» и технологиями имплантации, получает доступ к информации об истории компании и используемых ею технологиях.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-6">
						<div class="table w100 h800">
							<div class="row">
								<div class="cell w50 case-textb">
									<div class="wraptwo  wow fadeInLeft">
										<div class="case-title">Посадочная страница</div>
										<div class="case-label">
											<div>Комфорт</div>
										</div>
										<div class="case-text">
											<p>Динамичный слайдер на сайте подробно раскрывает структуру и порядок лечения. Посетитель знакомится с этапами лечения и избавляется от стоматологических страхов.</p>
										</div>
									</div>
								</div>
								<div class="cell w50 case-img">
									<div class="wraptwo"><img src="/assets/img/nobel/case-3.png" alt=""></div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-9">
						<div class="table w100 h800">
							<div class="row">
								<div class="cell w50 case-img">
									<div class="wraptwo"><img src="/assets/img/nobel/case-4.png" alt=""></div>
								</div>
								<div class="cell w50 case-textb">
									<div class="wraptwo  wow fadeInRight">
										<div class="case-title">Посадочная страница</div>
										<div class="case-label">
											<div>Доверительность</div>
										</div>
										<div class="case-text">
											<p>Посадочная страница рассказывает не только о лечении: она демонстрирует информацию о врачах, которые работают в компании «Интан», что формирует атмосферу спокойствия, профессионализма и доверия.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-6">
						<div class="table w100 h800">
							<div class="row">
								<div class="cell w50 case-textb">
									<div class="wraptwo  wow fadeInLeft">
										<div class="case-title">Посадочная страница</div>
										<div class="case-label">
											<div>Сбор информации</div>
										</div>
										<div class="case-text">
											<p>Небольшой удобный тест провоцирует посетителя пройти его, а компании позволяет получить важную маркетинговую информацию о своих потенциальных клиентах.</p>
										</div>
									</div>
								</div>
								<div class="cell w50 case-img">
									<div class="wraptwo"><img src="/assets/img/nobel/case-5.png" alt=""></div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-9">
						<div class="table w100 h800">
							<div class="row">
								<div class="cell w50 case-img">
									<div class="wraptwo"><img src="/assets/img/nobel/case-6.png" alt=""></div>
								</div>
								<div class="cell w50 case-textb">
									<div class="wraptwo  wow fadeInRight">
										<div class="case-title">Посадочная страница</div>
										<div class="case-label">
											<div>Доступность</div>
										</div>
										<div class="case-text">
											<p>Сайт предоставляет возможность быстрого перехода к записи на лечение. На странице несколько раз представлен быстрый переход к записи. Он удобен и привлекает дополнительное внимание пользователя.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-12 center">
							<img src="/assets/img/nobel/case-7.png" alt="">
					</div>
					<div class="case-19">
						<div class="wrap center">
							<!-- Slider main container-->
							<div class="swiper-container">
								<!-- Additional required wrapper-->
								<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/slides.php');?>
								<!-- If we need navigation buttons-->
								<div class="swiper-button-prev"></div>
								<div class="swiper-button-next"></div>
							</div>
						</div>
					</div>
					<div class="services-3">
						<div style="background-image: url(/assets/img/mda_bottom.jpg);" class="wrap">
							<div class="services-3-title">обратная связь</div>
							<div class="services-3-about">
								<div class="services-3-about-title">О возможных противопоказаниях </div>
								<div class="services-3-about-text">проконсультируйтесь<br>у нашего специалиста </div>
								<div class="services-3-about-btn"><a href="#order" class="btn blue fancy">Заказать услугу</a></div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/footer.php');?>

			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/modal.php');?>

			<!-- import scripts -->
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/scripts.php');?>
		</div>
	</body>
</html>
