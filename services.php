<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="shortcut icon" type="image/x-icon" href="/assets/img/favicon.ico">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="format-detection" content="telephone=no">

		<title>Создание сайтов, landing page, интернет-магазинов, порталов, социальных сетей в сфере оказания медицинских услуг</title>

		<meta name="description" content="Специализированная веб-разработка для различных медицинских учреждений: клиники, аптеки, стоматологии, медцентры. Цены от 40 000 рублей. Выполнение задач любой категории сложности.">
		<meta name="keywords" content="создание сайтов, landing page, интернет-магазинов, порталов">

		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/link_head.php');?>
		<!--if lt IE 9
		script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
		script(src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')
		-->
		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/yMetrika.php');?>
	</head>
	<body>
		<div id="page" class="page">
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/header.php');?>
			<section id="content" class="content" style="background: #f8f8f8;">
				<div class="services-nav top-nav">
					<div class="wrap">
						<div class="table w100">
							<div class="row">
								<div class="cell"> <a href="/services.php" parent="/services.php" class="active"><span>ВЕБ-РАЗРАБОТКА </span></a></div>
								<div class="cell"><a href="/marketing.php" parent="/services.php"><span>ИНТЕРНЕТ-МАРКЕТИНГ</span></a></div>
								<div class="cell"><a href="/brending.php" parent="/services.php"><span>БРЕНДИНГ</span></a></div>
								<div class="cell"><a href="/mobile.php" parent="/services.php"><span>МОБИЛЬНЫЕ ПРИЛОЖЕНИЯ</span></a></div>
							</div>
						</div>
					</div>
				</div>
				<div class="services-1">
					<div class="wrap">
						<div class="services-1-items">
							<div class="services-1-item"><b>Сайт</b> многопрофильной клиники</div>
							<div class="services-1-item"><b>Лендинг</b> об имплантации зубов</div>
							<div class="services-1-item"><b>База знаний</b> о лекарстве</div>
						</div>
						<div class="services-1-text">
							<p>
								Каждый сайт для нас уникален: мы вкладываем в него время, мысли, идеи, а наши проектировщики и разработчики обладают богатым опытом именно в области здравоохранения, что позволяет нам  не повторяться в решениях, создавая качественные и удобные площадки для привлечения клиентов.

							</p>
						</div>
						<div class="services-1-btn center"><a href="#order" class="ib btn blue fancy">Заказать услугу</a><a href="/cases.php" style="background-color: white" class="ib btn blue light fancy">портфолио</a></div>
					</div>
				</div>
				<div class="services-2">
					<div class="wrap">
						<div class="table">
							<div class="row"><a href="#web" class="cell w25 services-2-item fancy wow fadeInLeft" data-wow-delay="0s">
									<div class="services-2-item-counter">01</div>
									<div class="services-2-item-about">
										<div class="services-2-item-title"><span>Лендинг </span></div>
										<div class="services-2-item-price">от 40 000 <i class="fa fa-rub"></i></div>
										<div class="services-2-item-date">1 неделя</div>
									</div></a><a href="#web" class="cell w25 services-2-item fancy wow fadeInLeft" data-wow-delay="0.1s">
									<div class="services-2-item-counter">02</div>
									<div class="services-2-item-about">
										<div class="services-2-item-title"><span>Сайт</span></div>
										<div class="services-2-item-price">от 160 000 <i class="fa fa-rub"></i></div>
										<div class="services-2-item-date">4 недели</div>
									</div></a><a href="#web" class="cell w25 services-2-item fancy wow fadeInLeft" data-wow-delay="0.2s">
									<div class="services-2-item-counter">03</div>
									<div class="services-2-item-about">
										<div class="services-2-item-title"><span>Интернет магазин</span></div>
										<div class="services-2-item-price">от 240 000 <i class="fa fa-rub"></i></div>
										<div class="services-2-item-date">8 недель</div>
									</div></a><a href="#web" class="cell w25 services-2-item fancy wow fadeInLeft" data-wow-delay="0.3s">
									<div class="services-2-item-counter">04</div>
									<div class="services-2-item-about">
										<div class="services-2-item-title"><span>Портал  </span></div>
										<div class="services-2-item-price">от 320 000 <i class="fa fa-rub"></i></div>
										<div class="services-2-item-date">8 недель</div>
									</div></a></div>
						</div>
					</div>
				</div>
				<div class="services-3">
					<div style="background-image: url(/assets/img/mda_bottom.jpg);" class="wrap">
						<div class="services-3-title">обратная связь</div>
						<div class="services-3-about">
							<div class="services-3-about-title">О возможных противопоказаниях </div>
							<div class="services-3-about-text">проконсультируйтесь<br>у нашего специалиста </div>
							<div class="services-3-about-btn"><a href="#order" class="btn blue fancy">Заказать услугу</a></div>
						</div>
					</div>
				</div>
			</section>
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/footer.php');?>

			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/modal.php');?>
			<!-- import scripts -->
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/scripts.php');?>
		</div>
	</body>
</html>
