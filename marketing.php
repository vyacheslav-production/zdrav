<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="shortcut icon" type="image/x-icon" href="/assets/img/favicon.ico">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="format-detection" content="telephone=no">

		<title>Продвижение сайтов (SEO), настройка и ведение контекстной рекламы в Яндекс. Директ и Adwords, SMM, SERM для сайтов медицинской тематики</title>

		<meta name="description" content="Огромный опыт настройке и ведении рекламных кампаний в сфере оказания мединскийх услуг. Большой штат специалистов по различным направлениям интернет-маркетинга. Цены от 30 000 рублей.">
		<meta name="keywords" content="продвижение сайтов, seo, smm, serm, контекстная реклама">

		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/link_head.php');?>
		<!--if lt IE 9
		script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
		script(src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')
		-->
		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/yMetrika.php');?>
	</head>
	<body>
		<div id="page" class="page">
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/header.php');?>
			<section id="content" class="content" style="background: #f8f8f8;">
				<div class="services-nav top-nav">
					<div class="wrap">
						<div class="table w100">
							<div class="row">
								<div class="cell"><a href="/services.php" parent="/services.php"><span>ВЕБ-РАЗРАБОТКА</span></a></div>
								<div class="cell"><a href="/marketing.php" parent="/services.php" class="active"><span>ИНТЕРНЕТ-МАРКЕТИНГ</span></a></div>
								<div class="cell"><a href="/brending.php" parent="/services.php"><span>БРЕНДИНГ</span></a></div>
								<div class="cell"><a href="/mobile.php" parent="/services.php"><span>МОБИЛЬНЫЕ ПРИЛОЖЕНИЯ</span></a></div>
							</div>
						</div>
					</div>
				</div>
				<div class="services-1">
					<div class="wrap">
						<div class="services-1-items">
							<div class="services-1-item"><b>Партизанский маркетинг</b> для лекарства</div>
							<div class="services-1-item"><b>Интернет-сообщества</b> для клиники</div>
							<div class="services-1-item"><b>Оптимизация сайта</b> препарата </div>
						</div>
						<div class="services-1-text">
							<p>Маркетинг для медицины и фармацевтики – это один из самых сложных видов продвижения. </p>
							<p>Наши специалисты постоянно повышают уровень своей юридической грамотности, для того чтобы не нарушить тот или иной федеральный закон во время реализации маркетинговой стратегии. </p>
							<p>Мы понимаем, насколько важна для компаний их репутация и реноме портфеля препаратов.</p>
							<p>Наши маркетологи творчески используют практически все инструменты маркетинга, применимые к фармацевтике и медицине, а их опыт позволяет оптимизировать затраты на реализацию рекламной кампании.</p>
						</div>
						<div class="services-1-btn center"><a href="#order" class="ib btn blue fancy">Заказать услугу</a><a href="/cases.php" style="background-color: white" class="ib btn blue light fancy">портфолио</a></div>
					</div>
				</div>
				<div class="services-2">
					<div class="wrap">
						<div class="table">
							<div class="row"><a href="#marketing" class="cell w25 services-2-item fancy wow fadeInLeft" data-wow-delay="0s">
									<div class="services-2-item-counter">01</div>
									<div class="services-2-item-about">
										<div class="services-2-item-title"><span>SEO </span></div>
										<div class="services-2-item-price">от 30 000 <i class="fa fa-rub"></i></div>
										<div class="services-2-item-date">в месяц</div>
									</div></a><a href="#marketing" class="cell w25 services-2-item fancy wow fadeInLeft" data-wow-delay="0.1s">
									<div class="services-2-item-counter">02</div>
									<div class="services-2-item-about">
										<div class="services-2-item-title"><span>SMM</span></div>
										<div class="services-2-item-price">от 30 000 <i class="fa fa-rub"></i></div>
										<div class="services-2-item-date">в месяц</div>
									</div></a><a href="#marketing" class="cell w25 services-2-item fancy wow fadeInLeft" data-wow-delay="0.2s">
									<div class="services-2-item-counter">03</div>
									<div class="services-2-item-about">
										<div class="services-2-item-title"><span>SERM</span></div>
										<div class="services-2-item-price">от 30 000 <i class="fa fa-rub"></i></div>
										<div class="services-2-item-date">в месяц</div>
									</div></a><a href="#marketing" class="cell w25 services-2-item fancy wow fadeInLeft" data-wow-delay="0.3s">
									<div class="services-2-item-counter">04</div>
									<div class="services-2-item-about">
										<div class="services-2-item-title"><span>Context  </span></div>
										<div class="services-2-item-price">от 30 000 <i class="fa fa-rub"></i></div>
										<div class="services-2-item-date">в месяц</div>
									</div></a></div>
						</div>
					</div>
				</div>
				<div class="services-3">
					<div style="background-image: url(/assets/img/mda_bottom.jpg);" class="wrap">
						<div class="services-3-title">обратная связь</div>
						<div class="services-3-about">
							<div class="services-3-about-title">О возможных противопоказаниях </div>
							<div class="services-3-about-text">проконсультируйтесь<br>у нашего специалиста </div>
							<div class="services-3-about-btn"><a href="#order" class="btn blue fancy">Заказать услугу</a></div>
						</div>
					</div>
				</div>
			</section>
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/footer.php');?>

			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/modal.php');?>
			<!-- import scripts -->
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/scripts.php');?>
		</div>
	</body>
</html>
