<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="shortcut icon" type="image/x-icon" href="/assets/img/favicon.ico">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="format-detection" content="telephone=no">

		<title>Контакты</title>
		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/link_head.php');?>
		<!--if lt IE 9
		script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
		script(src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')
		-->
		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/yMetrika.php');?>
	</head>
	<body>
		<div id="page" class="page">
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/header.php');?>
			<section id="content" class="content">
				<div class="contacts-1">
					<div style="background-image: url(/assets/img/mda_contacts.jpg);" class="wrap">
						<div class="contacts-1-text">Мы открыты новым идеям, общению и будем рады новым знакомствам. Например, можно встретиться и обсудить ваш грандиозный проект за чашечкой кофе.</div>
					</div>
				</div>
				<div class="contacts-2">
					<div class="wrap">
						<div id="map" style="background-image: none" class="map"></div>
						<div class="contacts-2-contacts">
							<div class="contacts-2-contacts-title">Контакты</div>
							<div class="contacts-2-contacts-links">
								<div><a href="tel:+78126040350">+7 (812) 604-03-50</a></div>
								<div><a href="mailto:info@legion.info">info@legion.info</a></div>
							</div>
							<div class="contacts-2-contacts-text">
								<div>191186,<br>
									Санкт-Петербург,<br>
									Большая Конюшенная 27,<br>
									5 этаж, мансарда
								</div>
							</div>
						</div>
						<div class="contacts-2-social"><a href="http://vk.com/legionspb" target="_blank" rel="nofollow"><i class="fa fa-vk"></i></a><a href="http://instagram.com/legion_digital" target="_blank" rel="nofollow"><i class="fa fa-instagram"></i></a><a href="https://www.facebook.com/legionspb" target="_blank" rel="nofollow"><i class="fa fa-facebook"></i></a></div>
					</div>
				</div>
				<div class="contacts-3">
					<div class="wrap">
						<div class="table tablerson">
							<div class="row">
								<div class="cell first contacts-3-title"><span>Заявка</span></div>
								<div class="cell who contacts-3-text"><b>У вас есть проект?</b>
									<div>Давайте обсудим его. Продумаем. И сделаем!</div>
								</div>
								<div class="cell tree contacts-3-btn"><a href="#order" class="btn fancy">ОСТАВИТЬ ЗАЯВКУ</a></div>
							</div>
						</div>
					</div>
				</div>
				<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?callback=initMap"></script>
				<script>
					function initMap(){
						var MY_MAPTYPE_ID = 'mystyle';
						var stylez = [
						     {
						        "featureType": "landscape",
						        "stylers": [
						            {
						                "saturation": -99
						            },
						            {
						                "lightness": 0
						            },
						            {
						                "visibility": "on"
						            }
						        ]
						    },
						    {
						        "featureType": "poi",
						        "stylers": [
						            {
						                "saturation": -100
						            },
						            {
						                "lightness": 51
						            },
						            {
						                "visibility": "simplified"
						            }
						        ]
						    },
						    {
						        "featureType": "road.highway",
						        "stylers": [
						            {
						                "saturation": -100
						            },
						            {
						                "visibility": "simplified"
						            }
						        ]
						    },
						    {
						        "featureType": "road.arterial",
						        "stylers": [
						            {
						                "saturation": -100
						            },
						            {
						                "lightness": 30
						            },
						            {
						                "visibility": "on"
						            }
						        ]
						    },
						    {
						        "featureType": "road.local",
						        "stylers": [
						            {
						                "saturation": -100
						            },
						            {
						                "lightness": 40
						            },
						            {
						                "visibility": "on"
						            }
						        ]
						    },

						    {
						        "featureType": "transit",
						        "stylers": [
						            {
						                "saturation": -100
						            },
						            {
						                "visibility": "simplified"
						            }
						        ]
						    },
						    {
						        "featureType": "administrative.province",
						        "stylers": [
						            {
						                "visibility": "off"
						            }
						        ]
						    },
						    {
						        "featureType": "transit",
						        "elementType" : "labels.text",
						        "stylers": [

						            {
						                "color": "#278696"
						            },
						            {
						                "saturation": 51
						            },
						            {
						                "lightness": 10
						            },

						            {
						                "visibility": "simplified"
						            }
						        ]
						    },
						    {
						        "featureType": "road.local",
						        "elementType" : "geometry",
						        "stylers": [
						           {
						                "saturation": -100
						            },
						            {
						                "lightness": 0
						            },
						            {
						                "visibility": "on"
						            }
						        ]
						    },
						    {
						        "featureType": "water",
						        "elementType": "labels",
						        "stylers": [
						            {
						                "visibility": "on"
						            },
						            {
						                "lightness": -50
						            },
						            {
						                "saturation": -101
						            }
						        ]
						    },
						    {
						        "featureType": "water",
						        "elementType": "geometry",
						        "stylers": [
						            {
						                "hue": "#ffff00"
						            },
						            {
						                "lightness": -34
						            },
						            {
						                "saturation": -99
						            }
						        ]
						    }
					    ];
						map = new google.maps.Map(document.getElementById('map'), {
							center: {lat: 59.9371497, lng: 30.3225488},
							zoom: 18,
							scrollwheel: false,
						 	mapTypeControlOptions: {
								mapTypeIds: [google.maps.MapTypeId.ROADMAP, MY_MAPTYPE_ID]
							},
							mapTypeId: MY_MAPTYPE_ID
						});
						var styledMapOptions = {
							name: "Мой стиль"
						};

						var jayzMapType = new google.maps.StyledMapType(stylez, styledMapOptions);


						map.mapTypes.set(MY_MAPTYPE_ID, jayzMapType);
						var image = "/assets/img/map_marker.png";
						var marker = new google.maps.Marker({
							icon: image
						});
						var marker = new google.maps.Marker({
							map: map,
						    animation: google.maps.Animation.DROP,
							position: {lat: 59.9371497, lng: 30.3225488},
							icon: image
						});
					}


				</script>
			</section>
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/footer.php');?>
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/modal.php');?>
			<!-- import scripts -->
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/scripts.php');?>
		</div>
	</body>
</html>
