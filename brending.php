<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="shortcut icon" type="image/x-icon" href="/assets/img/favicon.ico">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="format-detection" content="telephone=no">

		<title>Брендинг, фирменный стиль, нейминг, графический дизайн для клиник, медцентров, стоматологий, аптек, поликлинк, больниц</title>

		<meta name="description" content="Большой опыт работы помогает нам с уверенностью сказать - мы знаем как правльно сделать графический дизайн для нужд медицинского учреждения, как правльно назвать препарат или услугу, как правильно подойти к оформлению. Цены на услуги от 40 000 рублей.">
		<meta name="keywords" content="брендинг, графический дизайн, нейминг, фирменный стиль">

		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/link_head.php');?>
		<!--if lt IE 9
		script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
		script(src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')
		-->
		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/yMetrika.php');?>
	</head>
	<body>
		<div id="page" class="page">
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/header.php');?>
			<section id="content" class="content" style="background: #f8f8f8;">
				<div class="services-nav top-nav">
					<div class="wrap">
						<div class="table w100">
							<div class="row">
								<div class="cell"><a href="/services.php" parent="/services.php"><span>ВЕБ-РАЗРАБОТКА</span></a></div>
								<div class="cell"><a href="/marketing.php" parent="/services.php"><span>ИНТЕРНЕТ-МАРКЕТИНГ</span></a></div>
								<div class="cell"> <a href="/brending.php" parent="/services.php" class="active"><span>БРЕНДИНГ</span></a></div>
								<div class="cell"><a href="/mobile.php" parent="/services.php"><span>МОБИЛЬНЫЕ ПРИЛОЖЕНИЯ</span></a></div>
							</div>
						</div>
					</div>
				</div>
				<div class="services-1">
					<div style="background: url(/assets/img/brending-bg.png) no-repeat center -315px;" class="wrap">
						<div class="services-1-items">
							<div class="services-1-item"><b>Нейминг</b> для стоматологии</div>
							<div class="services-1-item"><b>Логотип</b> клиники</div>
							<div class="services-1-item"><b>упаковка</b> препарата</div>
						</div>
						<div class="services-1-text">
							<p>Наши дизайнеры понимают, какие цвета и формы можно использовать в такой деликатной отрасли, как медицина.</p>
							<p>Проектные менеджеры знают, какая необходима информация на упаковке, чтобы пройти государственную экспертизу. </p>
							<p>Креатор осознает, как формируются названия, которые не режут слух пациенту и правильно отражают суть лекарства.</p>
							<p>Эти характеристики команды позитивно влияют как на качество, так и на сроки работы.</p>
						</div>
						<div class="services-1-btn center"><a href="#order" class="ib btn blue fancy">Заказать услугу</a><a href="/cases.php" style="background-color: white" class="ib btn blue light fancy">портфолио</a></div>
					</div>
				</div>
				<div class="services-2">
					<div class="wrap">
						<div class="table">
							<div class="row"><a href="#brending" class="cell w25 services-2-item fancy wow fadeInLeft" data-wow-delay="0s">
									<div class="services-2-item-counter">01</div>
									<div class="services-2-item-about">
										<div class="services-2-item-title"><span>Нейминг </span></div>
										<div class="services-2-item-price">от 40 000 <i class="fa fa-rub"></i></div>
										<div class="services-2-item-date">1 неделя</div>
									</div></a><a href="#brending" class="cell w25 services-2-item fancy wow fadeInLeft" data-wow-delay="0.1s">
									<div class="services-2-item-counter">02</div>
									<div class="services-2-item-about">
										<div class="services-2-item-title"><span>Фирменный стиль </span></div>
										<div class="services-2-item-price">от 100 000 <i class="fa fa-rub"></i></div>
										<div class="services-2-item-date">2 недели</div>
									</div></a><a href="#brending" class="cell w25 services-2-item fancy wow fadeInLeft" data-wow-delay="0.2s">
									<div class="services-2-item-counter">03</div>
									<div class="services-2-item-about">
										<div class="services-2-item-title"><span>Брошюра </span></div>
										<div class="services-2-item-price">от 80 000 <i class="fa fa-rub"></i></div>
										<div class="services-2-item-date">2 недели</div>
									</div></a><a href="#brending" class="cell w25 services-2-item fancy wow fadeInLeft" data-wow-delay="0.3s">
									<div class="services-2-item-counter">04</div>
									<div class="services-2-item-about">
										<div class="services-2-item-title"> <span>Упаковка  </span></div>
										<div class="services-2-item-price">от 80 000 <i class="fa fa-rub"></i></div>
										<div class="services-2-item-date">2 недели</div>
									</div></a></div>
						</div>
					</div>
				</div>
				<div class="services-3">
					<div style="background-image: url(/assets/img/mda_bottom.jpg);" class="wrap">
						<div class="services-3-title">обратная связь</div>
						<div class="services-3-about">
							<div class="services-3-about-title">О возможных противопоказаниях </div>
							<div class="services-3-about-text">проконсультируйтесь<br>у нашего специалиста </div>
							<div class="services-3-about-btn"><a href="#order" class="btn blue fancy">Заказать услугу</a></div>
						</div>
					</div>
				</div>
			</section>
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/footer.php');?>

			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/modal.php');?>

			<!-- import scripts -->
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/scripts.php');?>
		</div>
	</body>
</html>
