<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<link rel="shortcut icon" type="image/x-icon" href="/assets/img/favicon.ico">

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="format-detection" content="telephone=no">

	<title>Продвижение сайтов (SEO), настройка и ведение контекстной рекламы в Яндекс. Директ и Adwords, SMM, SERM для сайтов медицинской тематики</title>

	<meta name="description" content="Огромный опыт настройке и ведении рекламных кампаний в сфере оказания мединскийх услуг. Большой штат специалистов по различным направлениям интернет-маркетинга. Цены от 30 000 рублей.">
	<meta name="keywords" content="продвижение сайтов, seo, smm, serm, контекстная реклама">

	<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/link_head.php');?>
	<!--if lt IE 9
	script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
	script(src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')
	-->
	<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/yMetrika.php');?>
</head>

<body>

	<div id="page" class="page">

		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/header.php');?>

		<section id="content" class="content" style="background: #f8f8f8;">

			<div class="services-1">
				<div class="wrap">
					<div class="services-1-items">
						<div class="services-1-item">Семинар</div>
						<div class="services-1-item">«<b>Технологии интернет-продвижения медицины и фармацевтики</b>»</div>
					</div>
					<div class="services-1-titles">
						<h4>Программа семинара:</h4>
						<h3><i>Технологии в области фармацевтики</i></h3>
						<h5>Темы для обсуждения:</h5>
					</div>
					<div class="services-1-text modified">
						<ol>
							<li>
								Разработка сайтов препаратов. Формируем правильные цели и задачи. Повышение эффективности сайтов.<br /> 
								Спикер: Сергеев Андрей, руководитель проектного офиса
							</li>
							<li>
								Перспективы рынка мобильной разработки.<br /> 
								Спикер: Богородицкий, Вячеслав, руководитель направления разработки digital-проектов
							</li>
							<li>
								Маркетинг лекарственных препаратов. Что можно делать, а что нельзя.<br /> 
								Спикер: Пономарева Анастасия, руководитель отдела интернет-маркетинга
							</li>
						</ol>
					</div>
					<div class="services-1-titles">
						<h3><i>Технологии в области медицины</i></h3>
						<h5>Темы для обсуждения:</h5>
					</div>
					<div class="services-1-text modified">
						<ol>
							<li>
								Как геопозиционирование помогает вашему бизнесу? От карт до сервисов.<br /> 
								Спикер: Пономарева Анастасия, руководитель отдела интернет-маркетинга
							</li>
							<li>
								Подводные камни и неожиданные бонусы контекстной рекламы.<br /> 
								Спикер: Гаркуша Евгений, специалист-контекстолог
							</li>
							<li>
								Как эффективно работать с социальными сетями и медиа.<br /> 
								Спикер: Малков Никита, smm-специалист
							</li>
						</ol>
					</div>
				</div>
			</div>

			<div class="services-2">
				<div class="wrap">
					<div class="table">
						<div class="row">
							<a href="#marketing" class="cell w25 services-2-item fancy wow fadeInLeft" data-wow-delay="0s">
								<div class="services-2-item-counter">01</div>
								<div class="services-2-item-about">
									<div class="services-2-item-title"><span>SEO </span></div>
									<div class="services-2-item-price">от 30 000 <i class="fa fa-rub"></i></div>
									<div class="services-2-item-date">в месяц</div>
								</div>
							</a>
							<a href="#marketing" class="cell w25 services-2-item fancy wow fadeInLeft" data-wow-delay="0.1s">
								<div class="services-2-item-counter">02</div>
								<div class="services-2-item-about">
									<div class="services-2-item-title"><span>SMM</span></div>
									<div class="services-2-item-price">от 30 000 <i class="fa fa-rub"></i></div>
									<div class="services-2-item-date">в месяц</div>
								</div>
							</a>
							<a href="#marketing" class="cell w25 services-2-item fancy wow fadeInLeft" data-wow-delay="0.2s">
								<div class="services-2-item-counter">03</div>
								<div class="services-2-item-about">
									<div class="services-2-item-title"><span>SERM</span></div>
									<div class="services-2-item-price">от 30 000 <i class="fa fa-rub"></i></div>
									<div class="services-2-item-date">в месяц</div>
								</div>
							</a>
							<a href="#marketing" class="cell w25 services-2-item fancy wow fadeInLeft" data-wow-delay="0.3s">
								<div class="services-2-item-counter">04</div>
								<div class="services-2-item-about">
									<div class="services-2-item-title"><span>Context  </span></div>
									<div class="services-2-item-price">от 30 000 <i class="fa fa-rub"></i></div>
									<div class="services-2-item-date">в месяц</div>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>

			<div class="services-3">
				<div style="background-image: url(/assets/img/mda_bottom.jpg);" class="wrap">
					<div class="services-3-title">обратная связь</div>
					<div class="services-3-about">
						<div class="services-3-about-title">Дата проведения:</div>
						<div class="services-3-about-text modified">
							21 октября. Начало в 17.00.<br /> 
							Место проведения:<br /> 
							БЦ «Медведь» ул. Большая Конюшенная, д. 27, 5 эт., мансарда<br /> 
							Вход свободный
						</div>
						<div class="services-3-about-btn"><a href="#order" class="btn blue fancy">Записаться</a></div>
					</div>
				</div>
			</div>

		</section>

		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/footer.php');?>

		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/modal.php');?>
		<!-- import scripts -->
		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/scripts.php');?>

	</div>

</body>
</html>