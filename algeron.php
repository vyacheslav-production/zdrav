<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="shortcut icon" type="image/x-icon" href="/assets/img/favicon.ico">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="format-detection" content="telephone=no">

		<title>Альгерон</title>
		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/link_head.php');?>
		<!--if lt IE 9
		script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
		script(src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')
		-->
		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/yMetrika.php');?>
	</head>
	<body>
		<div id="page" class="page">
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/header.php');?>
			<section id="content" class="content">
				<div class="algeron">
					<div style="background-image: url(/assets/img/case/1-min.png); background-repeat: no-repeat; background-position: center" class="case-1">
						<div style=" " class="wrap">
							<div class="table">
								<div class="row">
									<div class="cell case-1-title"><span>кейс</span></div>
									<div class="cell case-1-about">
										<div>Комплексное продвижение препарата</div><b>Альгерон</b>
									</div>
								</div>
								<div class="row">
									<div class="cell case-1-title"><span>клиент</span></div>
									<div class="cell case-1-about"><img src="/assets/img/case/18-min.png" alt=""></div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-2">
						<div class="wrap">
							<div class="table w100">
								<div class="row">
									<div class="cell w33 wow fadeInLeft" data-wow-delay="0s">
										<div class="cases-2-item">
											<div class="index-6-couter">01</div>
											<div class="ib">
												<div class="cases-2-item-title">Задача</div>
												<div class="cases-2-item-text">Комплексное продвижение препарата Альгерон® на российском рынке биофармацевтики.</div>
											</div>
										</div>
									</div>
									<div class="cell w33 wow fadeInLeft" data-wow-delay="0.1s">
										<div class="cases-2-item">
											<div class="index-6-couter">02</div>
											<div class="ib">
												<div class="cases-2-item-title">Концепция</div>
												<div class="cases-2-item-text">Сделать введение препарата максимально простым, а обсуждение болезни пациентами максимально комфортным. </div>
											</div>
										</div>
									</div>
									<div class="cell w33 wow fadeInLeft" data-wow-delay="0.2s">
										<div class="cases-2-item">
											<div class="index-6-couter">03</div>
											<div class="ib">
												<div class="cases-2-item-title">Решение</div>
												<div class="cases-2-item-text">Нейминг и новый дизайн упаковки препарата, а также портал дла общения пациентов и врачей.</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div style="background-color: #fff;" class="case-3">
						<div style="background-image: url(/assets/img/case/big-1-min.png); background-color: #D3E0EA; background-repeat: no-repeat; background-position: center" class="wrap"></div>
					</div>
					<div class="case-4">
						<div style="background-image: url(/assets/img/case/3.png)" class="wrap">
							<div class="case-wrap  wow fadeInRight">
								<div class="case-title">Графический дизайн</div>
								<div class="case-label">
									<div>создание упаковки</div><b>препарата</b>
								</div>
								<div class="case-text">
									<p>Стояла задача познакомить врачей и пациентов с простым способом введения лекарства с помощью шприца. </p>
									<p>
										Упаковка должна наглядно и доступно демонстрировать информацию о дозировке и объеме препарата.


									</p>
								</div>
							</div>
						</div>
					</div>
					<div style="background: white" class="case-5">
						<div style="background-image: url(/assets/img/case/big-2-min.png); background-repeat: no-repeat; background-position: center" class="wrap"></div>
					</div>
					<div class="case-6">
						<div class="table w100 h800">
							<div class="row">
								<div class="cell w50 case-textb">
									<div class="wraptwo  wow fadeInLeft">
										<div class="case-title">портал<br>миродин</div>
										<div class="case-label">
											<div>социальная</div><b>сеть</b>
										</div>
										<div class="case-text">
											<p>Портал смог дать людям возможность не только получать консультации по интересующим их вопросам, но и делиться новостями из своей жизни, находить новых друзей, помогать другим людям.</p>
										</div>
									</div>
								</div>
								<div class="cell w50 case-img">
									<div class="wraptwo"><img src="/assets/img/case/6-min.png" alt=""></div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-7">
						<div class="table w100 h800">
							<div class="row">
								<div class="cell w50 case-img">
									<div class="wraptwo"><img src="/assets/img/case/9-min.png" alt=""></div>
								</div>
								<div class="cell w50 case-textb">
									<div class="wraptwo  wow fadeInRight">
										<div class="case-title">портал<br>миродин</div>
										<div class="case-label">
											<div>Информационный </div><b>портал</b>
										</div>
										<div class="case-text">
											<p>Профессиональные консультанты с медицинским образованием оперативно отвечают на ваши вопросы  и делятся своим экспертным мнением.</p>
											<p>
												Здесь нет «интернет-доктора»: посетителей консультируют практикующие врачи.



											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div style="background: white" class="case-8">
						<div style="background-image: url(/assets/img/case/big-3-min.png); background-repeat: no-repeat; background-position: center" class="wrap"></div>
					</div>
					<div class="case-9">
						<div class="table w100 h800">
							<div class="row">
								<div class="cell w50 case-textb">
									<div class="wraptwo  wow fadeInLeft">
										<div class="case-title">промо-сайт</div>
										<div class="case-label">
											<div>Информация </div><b>о препарате</b>
										</div>
										<div class="case-text">
											<p>На промо-сайте можно познакомиться со всеми нюансами, касающимися препарата: состав, инструкция по применению, адреса медицинских центров для приобретения препарата и прохождения лечения.</p>
										</div>
									</div>
								</div>
								<div class="cell w50 case-img">
									<div class="wraptwo"><img src="/assets/img/case/11-min.png" alt=""></div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-10">
						<div class="table w100 h800">
							<div class="row">
								<div class="cell w50 case-img">
									<div class="wraptwo"><img src="/assets/img/case/12-min.png" alt=""></div>
								</div>
								<div class="cell w50 case-textb">
									<div class="wraptwo  wow fadeInRight">
										<div class="case-title">мобильное<br>приложение</div>
										<div class="case-label">
											<div>Дневник</div><b>пациента</b>
										</div>
										<div class="case-text">
											<p>Мы разработали бесплатное приложение для iOS, позволяющее вести дневник с первого дня терапии и планировать собственное лечение.</p>
											<p>Приложение работает без подключения к сети, напоминает о визите к врачу или необходимости принять лекарство.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-10-1">
						<div class="table w100 h800">
							<div class="row">
								<div class="cell w50 case-textb">
									<div class="wraptwo wow fadeInLeft">
										<div class="case-title">мобильное<br>приложение</div>
										<div class="case-label">
											<div>динамика</div><b>лечения</b>
										</div>
										<div class="case-text">
											<p>На основе данных о приеме препарата и полученных  анализах для каждого инфицированного гепатитом С составляются индивидуальные графики.</p>
											<p>Состояние здоровья пациента отслеживается в динамике, позволяя отследить любые изменения.</p>
										</div><a href="https://itunes.apple.com/ru/app/al-geron-dnevnik-pacienta/id973820029?mt=8" target="_blank" class="appstore"><img src="/assets/img/case/19-min.png" alt=""></a>
									</div>
								</div>
								<div class="cell w50 case-img">
									<div class="wraptwo"><img src="/assets/img/case/13-min.png" alt=""></div>
								</div>
							</div>
						</div>
					</div>
					<div style="background: white" class="case-11">
						<div style="background-image: url(/assets/img/case/big-4-min.png); background-repeat: no-repeat; background-position: center" class="wrap"></div>
					</div>
					<div class="case-12 center  wow fadeInUp">
						<div style="background-image: url()" class="wrap"><img src="/assets/img/fishs.png" alt=""></div>
					</div>
					<div class="case-13">
						<div class="table w100 h800">
							<div class="row">
								<div class="cell w50 case-img">
									<div class="wraptwo"><img src="/assets/img/case-1.png" alt=""></div>
								</div>
								<div class="cell w50 case-textb">
									<div class="wraptwo  wow fadeInRight">
										<div class="case-title">Инструменты</div>
										<div class="case-label">
											<div>рост</div><b>трафика</b>
										</div>
										<div class="case-text">
											<p>Инициация обсуждений и комментарии специалистов на форумах помогли сгенерировать дополнительный трафик на сайт.   </p>
										</div>
										<div class="case-label">
											<div>обратная</div><b>связь</b>
										</div>
										<div class="case-text">
											<p>
												Привлечение медицинских работников к обратной связи с пользователями повысило доверие к ресурсу.

											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-14">
						<div class="table w100 h800">
							<div class="row">
								<div class="cell w50 case-textb">
									<div class="wraptwo  wow fadeInLeft">
										<div class="case-title">Инструменты</div>
										<div class="case-label">
											<div>рекламные </div><b>кампании</b>
										</div>
										<div class="case-text">
											<p>Благодаря точной настройке рекламных кампаний удалось собрать около 9000 подписчиков.</p>
										</div>
										<div class="case-label">
											<div>органика </div><b>и таргетинг</b>
										</div>
										<div class="case-text">
											<p>Настроены преимущественно на пользователей  из группы риска заболевания гепатитом С. </p>
										</div>
									</div>
								</div>
								<div style="background-image: url(/assets/img/case-2.png)" class="cell w50 case-img">
									<div class="wraptwo"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-15">
						<div class="table w100 h800">
							<div class="row">
								<div style="background-image: url(/assets/img/case/15-min.png); background-position: left top" class="cell w50 case-img">
									<div class="wraptwo">
										<div class="case-social"><a href="https://vk.com/mirodin_vk" target="_blank"><img src="/assets/img/case/32-min.png" alt=""></a><a href="https://www.facebook.com/portalmirodin/?fref=ts" target="_blank"><img src="/assets/img/case/33-min.png" alt=""></a></div>
									</div>
								</div>
								<div class="cell w50 case-textb">
									<div class="wraptwo  wow fadeInRight">
										<div class="case-title">smm</div>
										<div class="case-label">
											<div>социальные </div><b>сети</b>
										</div>
										<div class="case-text">
											<p>Через социальные сети Вконтакте и Facebook велось регулярное распространение контента об особенностях заболевания и способах его лечения.</p>
											<p>Инициация обсуждений и комментарии специалистов на форумах помогли заполнить информационное поле и сформировать устойчивое мнение об эффективности препарата.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-16 center"><img src="/assets/img/case-3.png" alt=""></div>
					<div class="case-17">
						<div class="table w100 h800">
							<div class="row">
								<div class="cell w50 case-textb">
									<div class="wraptwo wow fadeInLeft">
										<div class="case-title">serm</div>
										<div class="case-label"><b>общая работа</b>
											<div>с репутацией</div>
										</div>
										<div class="case-text">
											<p>Работа с упоминаниями в рамках SERM помогла сформировать положительное мнение о новом препарате до появления массового спроса на него.</p>
											<p>Уже на первых этапах работы был создан поисковый спрос на препарат.</p>
										</div>
									</div>
								</div>
								<div style="background-image: url(/assets/img/case/16-min.png)" class="cell w50 case-img">
									<div class="wraptwo"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-21 center"><img src="/assets/img/case-4.png" alt=""></div>
					<div class="case-20">
						<div style="background-image: url(/assets/img/case/17-min.png)" class="wrap center">миродин:<br>Вы не одиноки!</div>
					</div>
					<div class="case-19">
						<div class="wrap center">
							<!-- Slider main container-->
							<div class="swiper-container">
								<!-- Additional required wrapper-->
								<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/slides.php');?>
								<!-- If we need navigation buttons-->
								<div class="swiper-button-prev"></div>
								<div class="swiper-button-next"></div>
							</div>
						</div>
					</div>
					<div class="services-3">
						<div style="background-image: url(/assets/img/mda_bottom.jpg);" class="wrap">
							<div class="services-3-title">обратная связь</div>
							<div class="services-3-about">
								<div class="services-3-about-title">О возможных противопоказаниях </div>
								<div class="services-3-about-text">проконсультируйтесь<br>у нашего специалиста </div>
								<div class="services-3-about-btn"><a href="#order" class="btn blue fancy">Заказать услугу</a></div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/footer.php');?>

			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/modal.php');?>

			<!-- import scripts -->
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/scripts.php');?>
		</div>
	</body>
</html>
