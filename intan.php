<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="shortcut icon" type="image/x-icon" href="/assets/img/favicon.ico">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="format-detection" content="telephone=no">

		<title>Интан</title>
		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/link_head.php');?>
		<!--if lt IE 9
		script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
		script(src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')
		-->
		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/yMetrika.php');?>
	</head>
	<body>
		<div id="page" class="page">
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/header.php');?>
			<section id="content" class="content">
				<div class="intan">
					<div style="background: url(/assets/img/intan/1.png) no-repeat bottom center" class="case-1">
						<div class="wrap">
							<div class="table">
								<div class="row">
									<div class="cell case-1-title"><span>кейс</span></div>
									<div class="cell case-1-about">
										<div class="white">визуальная коммуникация бренда</div><b class="white">интан</b>
									</div>
								</div>
								<div class="row">
									<div style="vertical-align: top;" class="cell case-1-title"><span>клиент</span></div>
									<div style="vertical-align: top;" class="cell case-1-about"><img src="/assets/img/intan/log.png" alt=""></div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-2">
						<div class="wrap">
							<div class="table w100">
								<div class="row">
									<div class="cell w33 wow fadeInLeft" data-wow-delay="0s">
										<div class="cases-2-item">
											<div class="index-6-couter">01</div>
											<div class="ib">
												<div class="cases-2-item-title">Задача</div>
												<div class="cases-2-item-text">Ребрендинг и повышение посещаемости сайта</div>
											</div>
										</div>
									</div>
									<div class="cell w33 wow fadeInLeft" data-wow-delay="0.1s">
										<div class="cases-2-item">
											<div class="index-6-couter">02</div>
											<div class="ib">
												<div class="cases-2-item-title">Концепция</div>
												<div class="cases-2-item-text">Новый фирменный стиль, отличающий компанию от конкурентов</div>
											</div>
										</div>
									</div>
									<div class="cell w33 wow fadeInLeft" data-wow-delay="0.2s">
										<div class="cases-2-item">
											<div class="index-6-couter">03</div>
											<div class="ib">
												<div class="cases-2-item-title">Решение</div>
												<div class="cases-2-item-text">Полная трансформация фирменного стиля и сайт с удобной навигацией</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div style="background-color: #fff;" class="case-3">
						<div style="background-image: url(/assets/img/intan/2.png);" class="wrap"></div>
					</div>
					<div class="case-6">
						<div class="table w100 h800">
							<div class="row">
								<div style="background: #BFDDD0" class="cell w50 case-textb">
									<div class="wraptwo  wow fadeInLeft">
										<div class="case-title">брендинг</div>
										<div class="case-label">
											<div>Логотип</div><b>сети клиник</b>
										</div>
										<div class="case-text">
											<p>Мы изменили знак, шрифт и основные цвета. Новый фирменный стиль получился более динамичным и удобным для восприятия.</p>
											<p>
												Сохранив философию бренда, мы изменили логотип. Сделали его ярче и приятнее.

											</p>
										</div>
									</div>
								</div>
								<div style="background-image: url(/assets/img/intan/3.png); background-size: 80%; background-position: center; background-repeat: no-repeat;" class="cell w50 case-img"></div>
							</div>
						</div>
					</div>
					<div class="case-7">
						<div class="table w100 h800">
							<div class="row">
								<div style="background-image: url(/assets/img/intan/4.png); background-size: 80%; background-position: center; background-repeat: no-repeat; background-color: #fff" class="cell w50 case-img">
									<div class="wraptwo"></div>
								</div>
								<div style="background: #E8E8E8" class="cell w50 case-textb">
									<div class="wraptwo wow fadeInRight">
										<div class="case-title">брендинг</div>
										<div class="case-label">
											<div>яркие</div><b>Иллюстрации</b>
										</div>
										<div class="case-text">
											<p>
												Были придуманы и нарисованы новые персонажи для буклетов и раскрасок: бобр Зубр и его друзья. Они быстро полюбились маленьким пациентам.


											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-9">
						<div class="table w100 h800">
							<div class="row">
								<div style="background: #E8E8E8" class="cell w50 case-textb">
									<div class="wraptwo wow fadeInLeft">
										<div class="case-title">брендинг</div>
										<div class="case-label">
											<div>рекламные</div><b>материалы</b>
										</div>
										<div class="case-text">
											<p>
												Был оформлен дизайн полиграфической продукции: комиксов для детей, ежемесячной газеты, карточки врача, буклетов, календарей и др.

											</p>
										</div>
									</div>
								</div>
								<div style="background-image: url(/assets/img/intan/5.png); background-size: cover; background-position: center; background-repeat: no-repeat;" class="cell w50 case-img"></div>
							</div>
						</div>
					</div>
					<div style="background-color: #fff;margin-bottom: -1px;" class="case-3">
						<div style="background-image: url(/assets/img/intan/6.png);margin-top: -1px;" class="wrap"></div>
					</div>
					<div class="case-7">
						<div class="table w100 h800">
							<div class="row">
								<div style="background: #E8E8E8" class="cell w50 case-textb">
									<div class="wraptwo wow fadeInLeft">
										<div class="case-title">Веб-разработка</div>
										<div class="case-label">
											<div>корпоративный</div><b>сайт</b>
										</div>
										<div class="case-text">
											<p>Сбалансированный сайт с удобной навигацией, который содержит полную информацию о компании и предоставляемых ею услугах. Его функционал позволяет получить консультацию о работе клиники, рассчитать стоимость процедур и записаться на прием. </p>
										</div>
									</div>
								</div>
								<div style="background-image: url(/assets/img/intan/7.png); background-size: cover; background-position: center; background-repeat: no-repeat; background-color: #fff" class="cell w50 case-img"></div>
							</div>
						</div>
					</div>
					<div class="case-9">
						<div class="table w100 h800">
							<div class="row">
								<div style="background-image: url(/assets/img/intan/8.png); background-size: 80%; background-position: center ; background-repeat: no-repeat;" class="cell w50 case-img"></div>
								<div style="background: #E8E8E8" class="cell w50 case-textb">
									<div class="wraptwo wow fadeInRight">
										<div class="case-title">Веб-разработка</div>
										<div class="case-label">
											<div>Посадочные </div><b>страницы</b>
										</div>
										<div class="case-text">
											<p>Под каждую акцию создаются посадочные страницы с описанием услуг и их стоимости, формой заявки и контактами.</p>
											<p>
												Посетитель промо-страницы может узнать о преимуществах компании, найти ближайший филиал клиники и выбрать лечащего врача.


											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div style="background-color: #fff;margin-bottom: -1px;" class="case-3">
						<div style="background-image: url(/assets/img/intan/9.png); background-repeat: no-repeat; margin-top: -1px;" class="wrap"></div>
					</div>
					<div style="background-color: #E8E8E8;margin-top: -2px;" class="case-12 center wrap"><img src="/assets/img/intan/10.png" alt=""></div>
					<div class="case-7">
						<div class="table w100 h800">
							<div class="row">
								<div style="background: #fff" class="cell w50 case-textb">
									<div class="wraptwo wow fadeInLeft">
										<div class="case-title">продвижение</div>
										<div class="case-label">
											<div>Контент-</div><b>маркетинг</b>
										</div>
										<div class="case-text">
											<p>Подготовка текстов для сайта, буклетов, ежемесячной газеты, карточек врачей, социальных медиа. Создание креативной концепции акций и фотоконкурсов по разным направлениям. </p>
										</div>
									</div>
								</div>
								<div style="background-image: url(/assets/img/intan/11.png);background-size: 670px 680px; background-repeat: no-repeat; background-color: #fff; background-position: -3px -1px;" class="cell w50 case-img"></div>
							</div>
						</div>
					</div>
					<div class="case-9">
						<div class="table w100 h800">
							<div class="row">
								<div style="background-image: url(/assets/img/intan/12.png); background-size: cover; background-position: center ; background-repeat: no-repeat;" class="cell w50 case-img"></div>
								<div style="background: #fff" class="cell w50 case-textb">
									<div class="wraptwo wow fadeInRight">
										<div class="case-title">продвижение</div>
										<div class="case-label">
											<div>Контекстная</div><b>реклама</b>
										</div>
										<div class="case-text">
											<p>
												Контекстная реклама в Яндекс и Google показывается по всем услугам, предоставляемым клиниками. Показы идут как на поисковых площадках, так и на сайтах-партнерах.

											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-7">
						<div class="table w100 h800">
							<div class="row">
								<div style="background: #fff" class="cell w50 case-textb">
									<div class="wraptwo wow fadeInLeft">
										<div class="case-title">smm</div>
										<div class="case-label">
											<div>социальные</div><b>сети</b>
										</div>
										<div class="case-text">
											<p>На площадках ВКонтакте, Одноклассники и Facebook поддерживается высокий уровень активности. Пользователи могут оперативно получить онлайн-консультацию и первыми узнать об акциях и спецпредложениях.</p>
											<p>
												В рамках кампании осуществлялось продвижение отдельных акций с помощью таргетированной рекламы в социальных сетях.

											</p>
										</div>
									</div>
								</div>
								<div style="vertical-align: top !important; background-image: url(/assets/img/intan/13.png);background-size: cover; background-repeat: no-repeat; background-color: #fff; background-position: -3px -1px;" class="cell w50 case-img">
									<div style="padding-top: 80px;" class="case-social"><a href="https://vk.com/intan" target="_blank"><img src="/assets/img/case/32.png" alt=""></a><a href="https://www.facebook.com/intanstomatology/" target="_blank"><img src="/assets/img/case/33.png" alt=""></a><a href="https://odnoklassniki.ru/group/52894927552599" target="_blank"><img src="/assets/img/case/ok.png" alt=""></a></div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-9">
						<div class="table w100 h800">
							<div class="row">
								<div style="background-image: url(/assets/img/intan/14.png); background-size: cover; background-position: center ; background-repeat: no-repeat;" class="cell w50 case-img"></div>
								<div style="background: #E8E8E8" class="cell w50 case-textb">
									<div class="wraptwo wow fadeInRight">
										<div class="case-title">продвижение</div>
										<div class="case-label">
											<div>Поисковое </div><b>продвижение</b>
										</div>
										<div class="case-text">
											<p>
												Подготовлены статьи, посвященные разным направлениям стоматологии, для размещения на сайте клиента и на сторонних ресурсах. Правильный подбор ключевых слов помог компании занять первые места в топе по многим запросам.


											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div style="background-color: #fff;margin-bottom: -1px;" class="case-3">
						<div style="background-image: url(/assets/img/intan/15.png); background-repeat: no-repeat; margin-top: -1px;" class="wrap"></div>
					</div>
					<div class="case-7">
						<div class="table w100 h800">
							<div class="row">
								<div style="background: #fff url(/assets/img/intan/16-1.png) no-repeat right 99px;" class="cell w50 case-textb">
									<div class="wraptwo  wow fadeInLeft">
										<div class="case-title">Продвижение</div>
										<div class="case-label">
											<div>наружная </div><b>реклама</b>
										</div>
										<div class="case-text">
											<p>
												Были проанализированы, протестированы и введены в действие такие каналы коммуникации, как: размещение наружной рекламы на автобусах, размещение рекламных щитов на улицах города, аудио-реклама и лайтбоксы в метро, реклама на ТВ.



											</p>
										</div>
									</div>
								</div>
								<div style="vertical-align: top !important; background-image: url(/assets/img/intan/16.png);background-size: cover; background-repeat: no-repeat; background-color: #fff; background-position: -3px -1px;" class="cell w50 case-img"></div>
							</div>
						</div>
					</div>
					<div class="case-9">
						<div style="margin-top: -1px;" class="table w100 h800">
							<div class="row">
								<div style="background-image: url(/assets/img/intan/17.png); background-size: cover; background-position: center ; background-repeat: no-repeat;" class="cell w50 case-img"></div>
								<div style="background: #BFDDD0" class="cell w50 case-textb">
									<div class="wraptwo  wow fadeInRight">
										<div class="case-title">продвижение</div>
										<div class="case-label">
											<div>реклама </div><b>в метро</b>
										</div>
										<div class="case-text">
											<p>
												В соответствии с рекламной стратегией клиента, в метро регулярно размещаются лайтбоксы с информацией о месторасположении клиник и об акциях, доступных пациентам. Эффективным дополнением к лайтбоксам служит аудиореклама, которая звучит на всех станциях метро в одно и тоже время.

											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-19">
						<div class="wrap center">
							<!-- Slider main container-->
							<div class="swiper-container">
								<!-- Additional required wrapper-->
								<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/slides.php');?>
								<!-- If we need navigation buttons-->
								<div class="swiper-button-prev"></div>
								<div class="swiper-button-next"></div>
							</div>
						</div>
					</div>
					<div class="services-3">
						<div style="background-image: url(/assets/img/mda_bottom.jpg);" class="wrap">
							<div class="services-3-title">обратная связь</div>
							<div class="services-3-about">
								<div class="services-3-about-title">О возможных противопоказаниях </div>
								<div class="services-3-about-text">проконсультируйтесь<br>у нашего специалиста </div>
								<div class="services-3-about-btn"><a href="#order" class="btn blue fancy">Заказать услугу</a></div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/footer.php');?>
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/modal.php');?>
			<!-- import scripts -->
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/scripts.php');?>
		</div>
	</body>
</html>
