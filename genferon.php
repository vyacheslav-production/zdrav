<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="shortcut icon" type="image/x-icon" href="/assets/img/favicon.ico">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="format-detection" content="telephone=no">

		<title>Генферон</title>
		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/link_head.php');?>
		<!--if lt IE 9
		script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
		script(src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')
		-->
		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/yMetrika.php');?>
	</head>
	<body>
		<div id="page" class="page">
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/header.php');?>
			<section id="content" class="content">
				<div class="gencomfort">
					<div style="background: url(/assets/img/gencomfort/1.png) no-repeat center" class="case-1">
						<div class="wrap">
							<div class="table">
								<div class="row">
									<div class="cell case-1-title"><span>кейс</span></div>
									<div class="cell case-1-about">
										<div>Комплексное продвижение препарата</div><b>генферон</b>
									</div>
								</div>
								<div class="row">
									<div style="vertical-align: top;" class="cell case-1-title"><span>клиент</span></div>
									<div style="vertical-align: top;" class="cell case-1-about"><img src="/assets/img/gencomfort/18.png" alt=""></div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-2">
						<div class="wrap">
							<div class="table w100">
								<div class="row">
									<div class="cell w33 wow fadeInLeft" data-wow-delay="0s">
										<div class="cases-2-item">
											<div class="index-6-couter">01</div>
											<div class="ib">
												<div class="cases-2-item-title">Задача</div>
												<div class="cases-2-item-text">Визуальная концепция для препаратов Генферон</div>
											</div>
										</div>
									</div>
									<div class="cell w33 wow fadeInLeft" data-wow-delay="0.1s">
										<div class="cases-2-item">
											<div class="index-6-couter">02</div>
											<div class="ib">
												<div class="cases-2-item-title">Концепция</div>
												<div class="cases-2-item-text">Цветовое и графическое разделение линеек препарата</div>
											</div>
										</div>
									</div>
									<div class="cell w33 wow fadeInLeft" data-wow-delay="0.2s">
										<div class="cases-2-item">
											<div class="index-6-couter">03</div>
											<div class="ib">
												<div class="cases-2-item-title">Решение</div>
												<div class="cases-2-item-text">Новые упаковки и персонажи для рекламной кампании</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div style="background-color: #fff;" class="case-3">
						<div style="background-image: url(/assets/img/gencomfort/2.png);" class="wrap"></div>
					</div>
					<div class="case-4">
						<div style="background-image: url(/assets/img/gencomfort/3.png); background-position: center left; background-repeat: no-repeat;" class="wrap">
							<div class="case-wrap  wow fadeInRight">
								<div class="case-title">брендинг</div>
								<div class="case-label">
									<div>оформление </div><b>упаковки</b>
								</div>
								<div class="case-text">
									<p>Упаковки различаются по цветам в зависимости от линейки. Добавлены иконки, разъясняющие назначение продукта. </p>
									<p>
										Благодаря этому покупателям стало проще найти нужный препарат.

									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="case-6">
						<div class="table w100 h800">
							<div class="row">
								<div style="background: #FFFFFF" class="cell w50 case-textb">
									<div class="wraptwo  wow fadeInLeft">
										<div class="case-title">брендинг</div>
										<div class="case-label">
											<div>Линейки</div><b>препарата</b>
										</div>
										<div class="case-text">
											<p>
												Препараты поделены на три линейки: назальные спреи, свечи для взрослых и специальные свечи для детей и беременных женщин. Линейки различаются формой и визуальным оформлением упаковки.

											</p>
										</div>
									</div>
								</div>
								<div style="background-image: url(/assets/img/gencomfort/4.png); background-size: cover; background-position: center; background-repeat: no-repeat;" class="cell w50 case-img">
									<div class="wraptwo"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-7">
						<div class="table w100 h800">
							<div class="row">
								<div style="background-image: url(/assets/img/gencomfort/5.png); background-size: cover; background-position: center; background-repeat: no-repeat;" class="cell w50 case-img">
									<div class="wraptwo"></div>
								</div>
								<div style="background: #fff" class="cell w50 case-textb">
									<div class="wraptwo  wow fadeInRight">
										<div class="case-title">брендинг</div>
										<div class="case-label">
											<div>Создание</div><b>персонажей</b>
										</div>
										<div class="case-text">
											<p>
												Были созданы иллюстрации персонажей для разных категорий пациентов: детей, семейных пар и беременных женщин.



											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-9">
						<div class="table w100 h800">
							<div class="row">
								<div style="background: #F4F1EA" class="cell w50 case-textb">
									<div class="wraptwo wow fadeInLeft">
										<div class="case-title">брендинг</div>
										<div class="case-label">
											<div>рекламные </div><b>материалы</b>
										</div>
										<div class="case-text">
											<p>Разработаны рекламные полиграфические материалы со специально созданными иллюстрациями. </p>
											<p>Иллюстрации соответствуют разным аудиториям и способствуют росту лояльности потребителей к продукту.</p>
										</div>
									</div>
								</div>
								<div style="background-image: url(/assets/img/gencomfort/6.png); background-size: cover; background-position: center; background-repeat: no-repeat;" class="cell w50 case-img"></div>
							</div>
						</div>
					</div>
					<div style="background: white" class="case-11">
						<div style="background-image: url(/assets/img/gencomfort/7.png); background-repeat: no-repeat; background-position: center" class="wrap"></div>
					</div>
					<div style="padding: 0" class="case-12 center  wow fadeInUp">
						<div style="height: 100%; background-image: url(/assets/img/gencomfort/8.png); background-repeat: no-repeat; background-position: center" class="wrap"></div>
					</div>
					<div class="case-13">
						<div class="table w100 h800">
							<div class="row">
								<div style="background: rgba(159, 208, 234, 0.25)" class="cell w50 case-textb">
									<div class="wraptwo  wow fadeInLeft">
										<div class="case-title">веб-разработка</div>
										<div class="case-label">
											<div>Промо-сайт</div><b>препаратов</b>
										</div>
										<div class="case-text">
											<p>В оформлении промо-сайта сохранена концепция разделения цветовых решений. </p>
											<p>У препарата каждой группы указаны преимущества, интересующие его аудиторию. </p>
										</div>
									</div>
								</div>
								<div style="background: #fff" class="cell w50 case-img">
									<div class="wraptwo"><img src="/assets/img/gencomfort/9.png" alt=""></div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-14">
						<div class="table w100 h800">
							<div class="row">
								<div style="background-image: url(/assets/img/gencomfort/10.png); background-size: cover; background-position: center; background-repeat: no-repeat;" class="cell w50 case-img"></div>
								<div style="background: rgba(159, 208, 234, 0.25)" class="cell w50 case-textb">
									<div class="wraptwo  wow fadeInRight">
										<div class="case-title">веб-разработка</div>
										<div class="case-label">
											<div>Обратная  </div><b>связь</b>
										</div>
										<div class="case-text">
											<p>На сайте представлены ответы на общие вопросы и на вопросы о каждой линейке отдельно. </p>
											<p>
												Пользователь может также посмотреть отзывы пациентов и специалистов по каждому препарату.


											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-19">
						<div class="wrap center">
							<!-- Slider main container-->
							<div class="swiper-container">
								<!-- Additional required wrapper-->
								<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/slides.php');?>
								<!-- If we need navigation buttons-->
								<div class="swiper-button-prev"></div>
								<div class="swiper-button-next"></div>
							</div>
						</div>
					</div>
					<div class="services-3">
						<div style="background-image: url(/assets/img/mda_bottom.jpg);" class="wrap">
							<div class="services-3-title">обратная связь</div>
							<div class="services-3-about">
								<div class="services-3-about-title">О возможных противопоказаниях </div>
								<div class="services-3-about-text">проконсультируйтесь<br>у нашего специалиста </div>
								<div class="services-3-about-btn"><a href="#order" class="btn blue fancy">Заказать услугу</a></div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/footer.php');?>
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/modal.php');?>
			<!-- import scripts -->
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/scripts.php');?>
		</div>
	</body>
</html>
