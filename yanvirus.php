<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="shortcut icon" type="image/x-icon" href="/assets/img/favicon.ico">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="format-detection" content="telephone=no">

		<title>Яквинус</title>
		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/link_head.php');?>
		<!--if lt IE 9
		script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
		script(src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')
		-->
		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/yMetrika.php');?>
	</head>
	<body>
		<div id="page" class="page">
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/header.php');?>
			<section id="content" class="content">
				<div class="yanvirus">
					<div style="background: url(/assets/img/yanvirus/1.png) no-repeat bottom center" class="case-1">
						<div class="wrap">
							<div class="table">
								<div class="row">
									<div class="cell case-1-title"><span>кейс</span></div>
									<div class="cell case-1-about">
										<div class="white">препарат для лечения болезни Бехтерева </div><b class="white">Яквинус</b>
									</div>
								</div>
								<div class="row">
									<div style="vertical-align: top;" class="cell case-1-title"><span>клиент</span></div>
									<div style="vertical-align: top;" class="cell case-1-about"><img src="/assets/img/yanvirus/log.png" alt=""></div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-2">
						<div class="wrap">
							<div class="table w100">
								<div class="row">
									<div class="cell w33 wow fadeInLeft" data-wow-delay="0s">
										<div class="cases-2-item">
											<div class="index-6-couter">01</div>
											<div class="ib">
												<div class="cases-2-item-title">Задача</div>
												<div class="cases-2-item-text">Создать единый источник информации для больных ревматоидным артритом</div>
											</div>
										</div>
									</div>
									<div class="cell w33 wow fadeInLeft" data-wow-delay="0.1s">
										<div class="cases-2-item">
											<div class="index-6-couter">02</div>
											<div class="ib">
												<div class="cases-2-item-title">Концепция</div>
												<div class="cases-2-item-text">Разделение сайта на две версии: для врачей и для пациентов</div>
											</div>
										</div>
									</div>
									<div class="cell w33 wow fadeInLeft" data-wow-delay="0.2s">
										<div class="cases-2-item">
											<div class="index-6-couter">03</div>
											<div class="ib">
												<div class="cases-2-item-title">Решение</div>
												<div class="cases-2-item-text">Информативный сайт, посвященный болезни Бехтерева и возможностях ее лечения</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div style="background-color: #fff;" class="case-3">
						<div style="background-image: url(/assets/img/yanvirus/2.png);" class="wrap"></div>
					</div>
					<div class="case-6">
						<div class="table w100 h800">
							<div class="row">
								<div style="background: #E1F1CD" class="cell w50 case-textb">
									<div class="wraptwo wow fadeInLeft">
										<div class="case-title">версия<br>для врачей</div>
										<div class="case-label">
											<div>Дисклеймер </div><b>врач/пациент</b>
										</div>
										<div class="case-text">
											<p>Сайт предполагает ограничение при доступе к специализированной информации: по умолчанию пользователь попадает в раздел для пациента, а для доступа к специализированной медицинской информации о препарате необходимо правильно заполнить анкету с профессиональными вопросами.</p>
											<p>Разные версии ресурса позволяют обеим сторонам получать необходимые им данные.</p>
										</div>
									</div>
								</div>
								<div style="background-image: url(/assets/img/yanvirus/3.png); background-size: cover; background-position: center; background-repeat: no-repeat;" class="cell w50 case-img"></div>
							</div>
						</div>
					</div>
					<div class="case-7">
						<div class="table w100 h800">
							<div class="row">
								<div style="background-image: url(/assets/img/yanvirus/4.png); background-size: 80%; background-position: center; background-repeat: no-repeat; background-color: #fff" class="cell w50 case-img">
									<div class="wraptwo"></div>
								</div>
								<div style="background: #E8E8E8" class="cell w50 case-textb">
									<div class="wraptwo wow fadeInRight">
										<div class="case-title">версия<br>для врачей</div>
										<div class="case-label">
											<div>Исследования </div><b>и статистика</b>
										</div>
										<div class="case-text">
											<p>
												Специалисты могут получить на портале профессиональную информацию о проводимых медицинских исследованиях и статистике эффективности препарата.


											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-9">
						<div class="table w100 h800">
							<div class="row">
								<div style="background: #E8E8E8" class="cell w50 case-textb">
									<div class="wraptwo wow fadeInLeft">
										<div class="case-title">версия<br>для врачей</div>
										<div class="case-label">
											<div>анонсы  </div><b>мероприятий</b>
										</div>
										<div class="case-text">
											<p>На сайте расположены анонсы конференций, лекций, семинаров и других медицинских мероприятий, посвященных артриту.</p>
											<p>
												 В прошедших и грядущих событиях легко ориентироваться с помощью специального календаря.

											</p>
										</div>
									</div>
								</div>
								<div style="background-image: url(/assets/img/yanvirus/5.png); background-size: 80%; background-position: 50% 20px; background-repeat: no-repeat;" class="cell w50 case-img"></div>
							</div>
						</div>
					</div>
					<div class="case-7">
						<div class="table w100 h800">
							<div class="row">
								<div style="background-image: url(/assets/img/yanvirus/6.png); background-size: initial; background-position: center; background-repeat: no-repeat; background-color: #fff" class="cell w50 case-img">
									<div class="wraptwo"></div>
								</div>
								<div style="background: #E1F1CD" class="cell w50 case-textb">
									<div class="wraptwo wow fadeInRight">
										<div class="case-title">версия<br>для пациентов</div>
										<div class="case-label">
											<div>портал  </div><b>для пациентов</b>
										</div>
										<div class="case-text">
											<p>В версии для пациентов пользователь может ознакомиться с полезными статьями о современных способах лечения ревматоидного артрита и о преимуществах использования препарата «Яквинус».</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-9">
						<div class="table w100 h800">
							<div class="row">
								<div style="background: #E8E8E8" class="cell w50 case-textb">
									<div class="wraptwo wow fadeInLeft">
										<div class="case-title">версия<br>для пациентов</div>
										<div class="case-label">
											<div>оценка  </div><b>здоровья</b>
										</div>
										<div class="case-text">
											<p>
												Пользователь может пройти интерактивный тест для диагностики состояния суставов и получить рекомендации для лечения или облегчения своего состояния. При выявлении серьезных нарушений программа рекомендует тестируемому обратиться к врачу.


											</p>
										</div>
									</div>
								</div>
								<div style="background-image: url(/assets/img/yanvirus/7.png); background-size: cover; background-position: center ; background-repeat: no-repeat;" class="cell w50 case-img"></div>
							</div>
						</div>
					</div>
					<div class="case-7">
						<div class="table w100 h800">
							<div class="row">
								<div style="background-image: url(/assets/img/yanvirus/8.png); background-size: 80%; background-position: center; background-repeat: no-repeat; background-color: #fff" class="cell w50 case-img">
									<div class="wraptwo"></div>
								</div>
								<div style="background: #E1F1CD" class="cell w50 case-textb">
									<div class="wraptwo wow fadeInRight">
										<div class="case-title">версия<br>для пациентов</div>
										<div class="case-label">
											<div>места  </div><b>продаж</b>
										</div>
										<div class="case-text">
											<p>
												На портале собраны адреса тех аптек, в которых представлен препарат для лечения ревматоидного артрита. С помощью карты легко найти ближайшую точку продаж.



											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-19">
						<div class="wrap center">
							<!-- Slider main container-->
							<div class="swiper-container">
								<!-- Additional required wrapper-->
								<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/slides.php');?>
								<!-- If we need navigation buttons-->
								<div class="swiper-button-prev"></div>
								<div class="swiper-button-next"></div>
							</div>
						</div>
					</div>
					<div class="services-3">
						<div style="background-image: url(/assets/img/mda_bottom.jpg);" class="wrap">
							<div class="services-3-title">обратная связь</div>
							<div class="services-3-about">
								<div class="services-3-about-title">О возможных противопоказаниях </div>
								<div class="services-3-about-text">проконсультируйтесь<br>у нашего специалиста </div>
								<div class="services-3-about-btn"><a href="#order" class="btn blue fancy">Заказать услугу</a></div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/footer.php');?>

			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/modal.php');?>
			<!-- import scripts -->
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/scripts.php');?>
		</div>
	</body>
</html>
