<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="shortcut icon" type="image/x-icon" href="/assets/img/favicon.ico">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="format-detection" content="telephone=no">

		<title>Алерана</title>
		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/link_head.php');?>
		<!--if lt IE 9
		script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
		script(src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')
		-->
		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/yMetrika.php');?>
	</head>
	<body>
		<div id="page" class="page">
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/header.php');?>
			<section id="content" class="content">
				<div class="alerana">
					<div style="background: url(/assets/img/alerana/1.png) no-repeat center" class="case-1">
						<div class="wrap">
							<div class="table">
								<div class="row">
									<div class="cell case-1-title"><span>кейс</span></div>
									<div class="cell case-1-about">
										<div class="white">Продукты для лечения выпадения волос</div><b class="white">алерана</b>
									</div>
								</div>
								<div class="row">
									<div style="vertical-align: top;" class="cell case-1-title"><span>клиент</span></div>
									<div style="vertical-align: top;" class="cell case-1-about"><img src="/assets/img/alerana/log.png" alt=""></div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-2">
						<div class="wrap">
							<div class="table w100">
								<div class="row">
									<div class="cell w33 wow fadeInLeft" data-wow-delay="0s">
										<div class="cases-2-item">
											<div class="index-6-couter">01</div>
											<div class="ib">
												<div class="cases-2-item-title">Задача</div>
												<div class="cases-2-item-text">Заменить сайт на современный, сохранив позиции в поисковой выдаче.</div>
											</div>
										</div>
									</div>
									<div class="cell w33 wow fadeInLeft" data-wow-delay="0.1s">
										<div class="cases-2-item">
											<div class="index-6-couter">02</div>
											<div class="ib">
												<div class="cases-2-item-title">Концепция</div>
												<div class="cases-2-item-text">Уникальный дизайн и интерактивный тест для подбора рекомендаций по уходу за волосами.</div>
											</div>
										</div>
									</div>
									<div class="cell w33 wow fadeInLeft" data-wow-delay="0.2s">
										<div class="cases-2-item">
											<div class="index-6-couter">03</div>
											<div class="ib">
												<div class="cases-2-item-title">Решение</div>
												<div class="cases-2-item-text">Красивый и информативый сайт о преимуществах серии ALERANA и сохранении здоровья волос.</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div style="background-color: #fff;" class="case-3">
						<div style="background-image: url(/assets/img/alerana/2.png);" class="wrap"></div>
					</div>
					<div class="case-4">
						<div style="background-image: url(/assets/img/alerana/3.png); background-position: center left; background-repeat: no-repeat;" class="wrap">
							<div class="case-wrap  wow fadeInRight">
								<div class="case-title">слайдер</div>
								<div class="case-label">
									<div>основные   </div><b>серии средств</b>
								</div>
								<div class="case-text">
									<p>В ассортименте компании присутствуют препараты для лечения, укрепления и стимулирования роста волос; витаминные комплексы и стимуляторы роста ресниц и бровей.</p>
								</div>
							</div>
						</div>
					</div>
					<div class="case-6">
						<div class="table w100 h800">
							<div class="row">
								<div style="background: #D2B6C2" class="cell w50 case-textb">
									<div class="wraptwo wow fadeInLeft">
										<div class="case-title">каталог</div>
										<div class="case-label">
											<div>интуитивный</div><b>каталог</b>
										</div>
										<div class="case-text">
											<p>
												Мы предложили простой и понятный каталог, который помогает пользователю легко найти нужный препарат. Препараты разделены на подкатегории, для каждой из которых создана специальная иконка.

											</p>
										</div>
									</div>
								</div>
								<div style="background-image: url(/assets/img/alerana/4.png); background-size: cover; background-position: center; background-repeat: no-repeat;" class="cell w50 case-img">
									<div class="wraptwo"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-7">
						<div class="table w100 h800">
							<div class="row">
								<div style="background-image: url(/assets/img/alerana/5.png); background-size: cover; background-position: center; background-repeat: no-repeat; background-color: #EED9C6" class="cell w50 case-img">
									<div class="wraptwo"></div>
								</div>
								<div style="background: #EED9C6" class="cell w50 case-textb">
									<div class="wraptwo  wow fadeInRight">
										<div class="case-title">о препарате</div>
										<div class="case-label">
											<div>наглядные </div><b>иллюстрации</b>
										</div>
										<div class="case-text">
											<p>
												Для демонстрации работы продукта были разработаны особые иллюстрации. Их простота и наглядность благоприятно повлияли на доверие аудитории к препарату.


											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-9">
						<div class="table w100 h800">
							<div class="row">
								<div style="background: #EED9C6" class="cell w50 case-textb">
									<div class="wraptwo  wow fadeInLeft">
										<div class="case-title">диагностика</div>
										<div class="case-label">
											<div>интерактивный  </div><b>тест</b>
										</div>
										<div class="case-text">
											<p>На сайте был размещен простой и информативный тест, помогающий потенциальному покупателю выбрать подходящее средство. Проведя онлайн-диагностику, пользователь легко мог понять, какой препарат лучше всего подходит ему для волос и кожи головы.</p>
										</div>
									</div>
								</div>
								<div style="background-image: url(/assets/img/alerana/6.png); background-size: cover; background-position: center; background-repeat: no-repeat;" class="cell w50 case-img"></div>
							</div>
						</div>
					</div>
					<div class="case-7">
						<div class="table w100 h800">
							<div class="row">
								<div style="background-image: url(/assets/img/alerana/17.png); background-size: cover; background-position: center; background-repeat: no-repeat; background-color: #fff" class="cell w50 case-img">
									<div class="wraptwo"></div>
								</div>
								<div style="background: #D2B6C2" class="cell w50 case-textb">
									<div class="wraptwo  wow fadeInRight">
										<div class="case-title">где купить</div>
										<div class="case-label">
											<div>удобная </div><b>карта</b>
										</div>
										<div class="case-text">
											<p>
												Карта демонстрирует местоположение аптек, в которых можно приобрести препарат. География продукта включает в себя огромное количество точек продаж в городах России и Казахстана.


											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="case-9">
						<div class="table w100 h800">
							<div class="row">
								<div style="background: #E8E8E8" class="cell w50 case-textb">
									<div class="wraptwo  wow fadeInLeft">
										<div class="case-title">реклама</div>
										<div class="case-label">
											<div>создание  </div><b>баннера</b>
										</div>
										<div class="case-text">
											<p>Специально для рекламной кампании бренда был разработан баннер, раскрывающий преимущества продукта и привлекающий новых клиентов.</p>
										</div>
									</div>
								</div>
								<div style="background-image: url(/assets/img/alerana/15.png); background-size: cover; background-position: center -40px; background-repeat: no-repeat;" class="cell w50 case-img"></div>
							</div>
						</div>
					</div>
					<div class="case-19">
						<div class="wrap center">
							<!-- Slider main container-->
							<div class="swiper-container">
								<!-- Additional required wrapper-->
								<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/slides.php');?>
								<!-- If we need navigation buttons-->
								<div class="swiper-button-prev"></div>
								<div class="swiper-button-next"></div>
							</div>
						</div>
					</div>
					<div class="services-3">
						<div style="background-image: url(/assets/img/mda_bottom.jpg);" class="wrap">
							<div class="services-3-title">обратная связь</div>
							<div class="services-3-about">
								<div class="services-3-about-title">О возможных противопоказаниях </div>
								<div class="services-3-about-text">проконсультируйтесь<br>у нашего специалиста </div>
								<div class="services-3-about-btn"><a href="#order" class="btn blue fancy">Заказать услугу</a></div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/footer.php');?>

			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/modal.php');?>

			<!-- import scripts -->
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/scripts.php');?>
		</div>
	</body>
</html>
