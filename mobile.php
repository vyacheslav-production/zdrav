<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="shortcut icon" type="image/x-icon" href="/assets/img/favicon.ico">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="format-detection" content="telephone=no">

		<title>Мобильные приложения любой словжности для медицинских учреждений: разработка и продвижение</title>

		<meta name="description" content="Разработка полезных и функциональных моблильных приложений для различных медицинских учреждений: клиники, поликлиники, он-лайн запись к врачу, аптеки, медуентры, стоматологии.">
		<meta name="keywords" content="создание мобильных приложенией, разработка мобильных приложений">

		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/link_head.php');?>
		<!--if lt IE 9
		script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
		script(src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')
		-->
		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/yMetrika.php');?>
	</head>
	<body>
		<div id="page" class="page">
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/header.php');?>
			<section id="content" class="content" style="background: #f8f8f8;">
				<div class="services-nav top-nav">
					<div class="wrap">
						<div class="table w100">
							<div class="row">
								<div class="cell"><a href="/services.php" parent="/services.php"><span>ВЕБ-РАЗРАБОТКА </span></a></div>
								<div class="cell"><a href="/marketing.php" parent="/services.php"><span>ИНТЕРНЕТ-МАРКЕТИНГ</span></a></div>
								<div class="cell"><a href="/brending.php" parent="/services.php"><span>БРЕНДИНГ</span></a></div>
								<div class="cell"><a href="/mobile.php" parent="/services.php" class="active"><span>МОБИЛЬНЫЕ ПРИЛОЖЕНИЯ</span></a></div>
							</div>
						</div>
					</div>
				</div>
				<div class="services-1">
					<div class="wrap">
						<div class="services-1-items">
							<div class="services-1-item"><b>Напоминание</b> о приеме лекарства</div>
							<div class="services-1-item"><b>Хранение</b> истории болезни</div>
							<div class="services-1-item"><b>Информация</b> о препарате</div>
						</div>
						<div class="services-1-text">
							<p>Мобильные приложения, у каждого из которых свой функционал и назначение, охватывают огромный круг важных для медицины и фармацевтики задач.</p>
							<p>Наша команда разрабатывала мобильные дневники пациента, базы знаний для медицинских представителей, клиенты к инфо-порталам. </p>
							<p>Опыт проектирования и разработки подобных приложений помогает нашим проектным менеджерам и разработчикам не совершать ошибок, критичных для сроков и стоимости проекта. </p>
						</div>
						<div class="services-1-btn center"> <a href="#order" class="ib btn blue fancy">Заказать услугу</a><a href="/cases.php" style="background-color: white" class="ib btn blue light fancy">портфолио</a></div>
					</div>
				</div>
				<div class="services-2">
					<div class="wrap">
						<div class="table">
							<div class="row"><a href="#mobile" class="cell w25 services-2-item fancy wow fadeInLeft" data-wow-delay="0s">
									<div class="services-2-item-counter">01</div>
									<div class="services-2-item-about">
										<div class="services-2-item-title smaller"><span>Корпоративные приложения </span></div>
										<div class="services-2-item-price">от 400 000 <i class="fa fa-rub"></i></div>
										<div class="services-2-item-date">3 месяца</div>
									</div></a><a href="#mobile" class="cell w25 services-2-item fancy wow fadeInLeft" data-wow-delay="0.1s">
									<div class="services-2-item-counter">02</div>
									<div class="services-2-item-about">
										<div class="services-2-item-title smaller"><span>Развлекательные приложения</span></div>
										<div class="services-2-item-price">от 400 000 <i class="fa fa-rub"></i></div>
										<div class="services-2-item-date">3 месяца</div>
									</div></a><a href="#mobile" class="cell w25 services-2-item fancy wow fadeInLeft" data-wow-delay="0.2s">
									<div class="services-2-item-counter">03</div>
									<div class="services-2-item-about">
										<div class="services-2-item-title smaller"><span>Клиентские приложения </span></div>
										<div class="services-2-item-price">от 400 000 <i class="fa fa-rub"></i></div>
										<div class="services-2-item-date">3 месяца</div>
									</div></a><a href="#mobile" class="cell w25 services-2-item fancy wow fadeInLeft" data-wow-delay="0.3s">
									<div class="services-2-item-counter">04</div>
									<div class="services-2-item-about">
										<div class="services-2-item-title smaller"><span>Брендированные приложения  </span></div>
										<div class="services-2-item-price">от 400 000 <i class="fa fa-rub"></i></div>
										<div class="services-2-item-date">3 месяца</div>
									</div></a></div>
						</div>
					</div>
				</div>
				<div class="services-3">
					<div style="background-image: url(/assets/img/mda_bottom.jpg);" class="wrap">
						<div class="services-3-title">обратная связь</div>
						<div class="services-3-about">
							<div class="services-3-about-title">О возможных противопоказаниях </div>
							<div class="services-3-about-text">проконсультируйтесь<br>у нашего специалиста </div>
							<div class="services-3-about-btn"><a href="#order" class="btn blue fancy">Заказать услугу</a></div>
						</div>
					</div>
				</div>
			</section>
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/footer.php');?>

			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/modal.php');?>
			<!-- import scripts -->
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/scripts.php');?>
		</div>
	</body>
</html>
