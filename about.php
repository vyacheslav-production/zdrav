<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="shortcut icon" type="image/x-icon" href="/assets/img/favicon.ico">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="format-detection" content="telephone=no">

		<title>Легион Здравоохранение - многолетний опыт в сфере рекламы медицинских товаров и услуг</title>
		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/link_head.php');?>
		<!--if lt IE 9
		script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
		script(src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')
		-->
		<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/yMetrika.php');?>
	</head>
	<body>
		<div id="page" class="page">
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/header.php');?>
			<section id="content" class="content">
				<div class="about">
					<div class="about-1">
						<div style="background-image: url(/assets/img/about/item-1-min.png)" class="wrap"></div>
					</div>
					<div class="about-2">
						<div class="wrap">
							<div class="table tablerson w100">
								<div class="row">
									<div class="cell first job-3-title"><span>О нас</span></div>
									<div class="cell wow fadeInRight">
										<div class="index-2-text-title">
											<div>цели  </div>
											<div><b>и задачи</b></div>
										</div>
										<p>Мы всегда работаем в команде, что приводит к максимально качественному результату. При этом стараемся сочетать инновационность наших идей со взвешенностью решений.</p><a href="#order" class="btn white light fancy">Присоединиться</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="about-3">
						<div class="wrap">
							<div class="job-3-title"><span>Наша команда</span></div>
							<div class="swiper-container">
								<div class="swiper-button-next"></div>
								<div class="swiper-wrapper">
									<div class="swiper-slide">
										<div class="about-3-img"><img src="/assets/img/about/4.png" alt=""></div>
										<div class="about-3-text">
											<div class="index-2-text-title">
												<div>АРТЁМ   </div>
												<div><b>КОЛЬЦОВ</b></div><i>Основатель</i>
											</div>
											<p>В институте мы занимались организацией вечеринок, постоянно приходилось придумывать что–то новое. Мне нравился процесс разработки концепций все больше, а вечеринки — все меньше. В 2007 году было зарегистрировано ООО «Легион».</p>
										</div>
									</div>
									<div class="swiper-slide">
										<div class="about-3-img"><img src="/assets/img/about/sofia.png" alt=""></div>
										<div class="about-3-text">
											<div class="index-2-text-title">
												<div>София   </div>
												<div><b>Будишевская</b></div><i>Арт-директор</i>
											</div>
											<p>
												В «Легион» я попала исключительно благодаря личному обаянию его основателя - Артема Кольцова. Он буквально осаждал меня несколько месяцев с предложениями работать у него в компании. Общаться с ним было легко, весело и непринужденно, поэтому я решила, что в «Легионе» царит подходящая для творчества атмосфера. И вот я здесь.
											</p>
										</div>
									</div>
									<div class="swiper-slide">
										<div class="about-3-img"><img src="/assets/img/about/nastia.png" alt=""></div>
										<div class="about-3-text">
											<div class="index-2-text-title">
												<div>Анастасия   </div>
												<div><b>Пономарева</b></div><i>Руководитель отдела интернет-маркетинга</i>
											</div>
											<p>
												Высшее образование я получила по специальности «связи с общественностью». <br>
												С 2012 работаю в сфере маркетинга в социальных сетях, как с B2C, так и с B2B клиентами. Сейчас специализируюсь на продвижении клиник и фармацевтики в интернете.
											</p>
										</div>
									</div>
									<div class="swiper-slide">
										<div class="about-3-img"><img src="/assets/img/about/nik.png" alt=""></div>
										<div class="about-3-text">
											<div class="index-2-text-title">
												<div>Никита   </div>
												<div><b>Малков</b></div><i>SMM-специалист</i>
											</div>
											<p>
												Уже 4 года работаю в области SMM, занимаюсь разработкой рекламных компаний и продвижением брендов в социальных сетях. Работал с компаниями из сфер недвижимости и банковского дела.<br> Сейчас сконцентрировался на медицинском маркетинге.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="about-4">
						<div style="background-image: url(/assets/img/about/house-1-min.png)" class="wrap">
							<div class="about-4-big-item">
								<div class="about-4-big-item-title">Брендинг в фармацевтике</div>
								<div class="about-4-big-item-btns"><a href="/upload/pdf/Brending.pdf" download="Brending.pdf" target="_blank"><i class="fa fa-file-pdf-o"></i><span>Скачать, </span><i>3.3 мб</i></a><a href="#SMMvidio" class="fancy"><i class="fa"></i><span>Смотреть</span></a></div>
								<div class="about-4-big-item-body">
									<!--div style="background-image: url(/assets/img/about/7.png)" class="about-4-big-item-ava"></div-->
									<div class="about-4-big-item-name index-2-text-title">
										<div>Медицина и </div>
										<div><b>фармацевтика</b></div>
									</div>
									<div class="about-4-big-item-social"><a href="https://vk.com/anastasia_ponomareva" target="_blank" rel="nofollow"><i class="fa fa-vk"></i></a><a href="https://www.facebook.com/anastasia1ponomareva" target="_blank" rel="nofollow"><i class="fa fa-facebook"></i></a>
									</div>
									<div class="about-4-big-item-namea"><span>Digital в области медицины</span></div>
								</div>
							</div>
							<div class="about-4-item first wow fadeInRight">
								<div><a href="http://www.slideshare.net/IALegion/serm-35259712" target="_blank">«работа с репутацией в медицине»</a></div><span><span>мнение эксперта</span></span>
							</div>
							<div class="about-4-item two wow fadeInLeft">
								<div><a href="http://www.slideshare.net/IALegion/4-smm-meditsina240315" target="_blank">«социальные медиа при работе с медицинской тематикой»</a></div><span><span>мнение эксперта</span></span>
							</div>
							<div class="about-4-item tree wow fadeInUp">
								<div><a href="http://www.slideshare.net/IALegion/ss-46254511" target="_blank">«мобильная разработка для фармацевтических компаний»</a></div><span><span>мнение эксперта</span></span>
							</div>
						</div>
					</div>
					<div class="about-5 groupslider">
						<div class="wrap">
							<div class="index-2-text-title">
								<div>Наши</div>
								<div><b>клиенты</b></div>
							</div>
							<div class="slider-clients">
								<!-- Slider main container-->
								<div class="swiper-container">
									<!-- Additional required wrapper-->
									<div class="swiper-wrapper">
										<!-- Slides-->
										<div class="swiper-slide"></div>
										<div class="swiper-slide"><img src="/assets/img/about/11.png" alt=""><img src="/assets/img/about/11_color.png" alt=""></div>
										<div class="swiper-slide"><img src="/assets/img/about/9.png" alt=""><img src="/assets/img/about/9_color.png" alt=""></div>
										<div class="swiper-slide"><img src="/assets/img/about/13.png" alt=""><img src="/assets/img/about/13_color.png" alt=""></div>
										<div class="swiper-slide"><img src="/assets/img/about/22.png" alt=""><img src="/assets/img/about/22_color.png" alt=""></div>
										<div class="swiper-slide"><img src="/assets/img/about/ver-1.png" alt=""><img src="/assets/img/about/ver.png" alt=""></div>
										<div class="swiper-slide"><img src="/assets/img/about/kladzdor.png" alt=""><img src="/assets/img/about/kladzdor_color.png" alt=""></div>
										<div class="swiper-slide"><img src="/assets/img/about/scandinavia.png" alt=""><img src="/assets/img/about/scandinavia_color.png" alt=""></div>
									</div>
								</div>
								<!-- If we need navigation buttons-->
								<div class="swiper-button-prev"></div>
								<div class="swiper-button-next"></div>
							</div>
						</div>
					</div>
					<div class="about-6">
						<div style="background-image: url(/assets/img/about/item-3-min.png)" class="wrap">
							<div class="about-6-text wow fadeInUp">
								<div class="about-6-text-title">1 место</div>
								<div class="about-6-text-text">В номинации<br>«Лучший офис года»</div>
								<div class="about-6-text-label"> <span>Премия<br>specia</span></div>
							</div>
							<div class="about-6-item first wow fadeInLeft">
								<strong>Семинары</strong><br>
								<span>и курсы</span>
							</div>
							<div class="about-6-item two wow fadeInDown">
								<strong>мастер</strong><br>
								<span>классы</span>
							</div>
							<div class="about-6-item three">
								<strong>площадка</strong><br>
								<span>под проект</span>
								<div class="about-6-text-label">
									Оборудованное<br>
									пространство <br>
									на 20-30 мест <br>
								</div>
								<a href="#order" class="btn white light fancy">подать заявку</a>
							</div>
						</div>
					</div>
					<div class="about-7 groupslider">
						<div class="wrap">
							<div class="index-2-text-title">
								<div>Наши</div>
								<div><b>награды</b></div>
							</div>
							<div class="slider-clients">
								<!-- Slider main container-->
								<div class="swiper-container">
									<!-- Additional required wrapper-->
									<div class="swiper-wrapper">
										<!-- Slides-->
										<div class="swiper-slide"></div>
										<div class="swiper-slide"><img src="/assets/img/about/nagrada.png" alt=""></div>
										<div class="swiper-slide"><img src="/assets/img/about/nagrada-1.png" alt=""></div>
										<div class="swiper-slide"><img src="/assets/img/about/nagrada-2.png" alt=""></div>
										<div class="swiper-slide"><img src="/assets/img/about/nagrada-3.png" alt=""></div>
										<div class="swiper-slide"><img src="/assets/img/about/nagrada-4.png" alt=""></div>
										<div class="swiper-slide"><img src="/assets/img/about/nagrada.png" alt=""></div>
										<div class="swiper-slide"><img src="/assets/img/about/nagrada-1.png" alt=""></div>
										<div class="swiper-slide"><img src="/assets/img/about/nagrada-2.png" alt=""></div>
										<div class="swiper-slide"><img src="/assets/img/about/nagrada-3.png" alt=""></div>
										<div class="swiper-slide"><img src="/assets/img/about/nagrada-4.png" alt=""></div>
									</div>
								</div>
								<!-- If we need navigation buttons-->
								<div class="swiper-button-prev"></div>
								<div class="swiper-button-next"></div>
							</div>
						</div>
					</div>
					<div class="about-8">
						<div style="background-image: url(/assets/img/about/house-2-min.png); background-size: cover" class="wrap">
							<div class="about-8-d wow fadeInLeft"><b>12</b><br><span>октября</span></div>
							<div class="about-8-d two wow fadeInDown"><b>20-21</b><br><span>октября</span></div>
							<div class="about-8-date">
								<div class="adter"><i class="fa"></i>
										<span><b>13</b>
										<div>марта</div></span></div>
								<div class="about-8-title">маркетинг в социальных медиа</div>
								<div class="about-8-label"><span> Анастасия Пономарева, руководитель отдела маркетинга</span></div>
							</div>
						</div>
					</div>
					<div class="about-9">
						<div class="wrap">
							<div class="table tablerson w100">
								<div class="row">
									<div class="cell first job-3-title"><span>пресс кит</span></div>
									<div class="cell">
										<div class="table w100">
											<div class="row">
												<div style="width: 340px;" class="cell index-2-text-title">
													<div>Презентация   </div>
													<div><b>компании</b></div>
												</div>
												<div class="cell to-pdf right"><a href="/upload/pdf/farm_prez.pdf" download="/upload/pdf/farm_prez.pdf" tagret="_blank" class="link-to-pdf left"><i class="fa fa-file-pdf-o"></i><span><span>На Русском</span><i>9.4 Мб</i></span></a><a href="/upload/pdf/Legion_final_eng.pdf" download="/upload/pdf/Legion_final_eng.pdf" tagret="_blank" class="link-to-pdf left"><i class="fa fa-file-pdf-o"></i><span><span>in english</span><i>6.5 Mb</i></span></a><a href="/upload/pdf/Legion_france.pdf" download="/upload/pdf/Legion_france.pdf" tagret="_blank" class="link-to-pdf left"><i class="fa fa-file-pdf-o"></i><span><span>en franÇaise</span><i>9.5 Mb</i></span></a></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/footer.php');?>

			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/modal.php');?>

			<div class="hidden" id="SMMvidio">
				<div class=class="job-3" style="padding: 80px;">
					<div class="job-form"><a href="#" class="fancy-close"><i class="fa fa-times"></i></a>
						<iframe width="640" height="480" src="https://www.youtube.com/embed/7-IfI1H2B8s" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
			</div>

			<!-- import scripts -->
			<?require_once($_SERVER['DOCUMENT_ROOT'].'/layouts/scripts.php');?>
		</div>
	</body>
</html>
